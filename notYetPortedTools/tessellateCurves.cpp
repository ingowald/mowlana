  pbrt::TriangleMesh::SP tessellate(pbrt::Curve::SP curve)
  {
    if (Renderer::dropCurves) return pbrt::TriangleMesh::SP ();
    
    pbrt::TriangleMesh::SP mesh = std::make_shared<pbrt::TriangleMesh>();
    mesh->material = curve->material;

    const affine3f xfm = (const affine3f&)curve->transform;
    const vec3f P0 = (const vec3f&)curve->P.front();
    const vec3f P1 = (const vec3f&)curve->P.back();

    // -------------------------------------------------------
    // compute a curve (bi-)normal
    // -------------------------------------------------------
    vec3f N = vec3f(0);
    for (int i=2;i<curve->P.size();i++) {
      const vec3f pi0 = (const vec3f&)curve->P[i-2];
      const vec3f pi1 = (const vec3f&)curve->P[i-1];
      const vec3f pi2 = (const vec3f&)curve->P[i-0];
      N += cross (pi2-pi1,pi1-pi0);
    }
    if (N != vec3f(0.f)) {
      N = normalize(N);
    } else {
      const vec3f L = normalize(P1-P0);
      N = vec3f(L.y,L.z,L.x);
    }

    // -------------------------------------------------------
    // determine how many verts & tris we'll have
    // assuming b-splines that do not interpolate the endpoints
    // -------------------------------------------------------
    const int numCurveCtlPts = curve->P.size();
    const int numCurveCtlPtsExceptLast = curve->P.size() - 1;

    const int samplesPerCtlPt = 1; // dave: 3
    const int numCtlSegments = curve->P.size() - 2 - 1;

    const int numCurveSamplesExceptLast = numCtlSegments * samplesPerCtlPt;
    const int numCurveSamples = numCurveSamplesExceptLast + 1;

    const int numMeshVerts = 2 * numCurveSamples;
    const int numMeshTris = 2 * numCurveSamplesExceptLast;


    mesh->vertex.reserve(numMeshVerts);
    mesh->index.reserve(numMeshTris);

    // pre-transform all points
    std::vector<vec3f> xp;
    xp.resize(numCurveCtlPts);
    for (int i = 0; i < numCurveCtlPts; i++) {
      xp[i] = (const vec3f&)curve->P[i];
      xp[i] = (const vec3f&)curve->P[i];
    }

    // build the mesh
    vec3f B; // binormal
    vec3f bez_P[4]; // bezier control points
    vec3f skelP, skelT; // evaluated curve position & tangent
    std::vector<vec3f>& meshVerts = (std::vector<vec3f>&)mesh->vertex;

    for (int sampleIdx = 0; sampleIdx < numCurveSamples; sampleIdx++) {
      const int ctlSegmentIdx = sampleIdx / samplesPerCtlPt; // round down
      const int ctlSegmentSampleIdx = ctlSegmentIdx * samplesPerCtlPt;
      const int ctlPtIdx = (sampleIdx < numCurveSamples - 1) 
        ? ctlSegmentIdx - 1 + 1
        : ctlSegmentIdx - 1 + 1 - 1;


      // control point indices should be in-bounds for curve->P
      assert( ctlPtIdx >= 0 );
      assert( ctlPtIdx+3 < curve->P.size() );

      // compute the bezier control points of this segment
      bspline_to_bezier( xp[ctlPtIdx], xp[ctlPtIdx+1], xp[ctlPtIdx+2], xp[ctlPtIdx+3], bez_P );

      // get segment & strand parameter
      const float t_segment = (float)(sampleIdx - ctlSegmentSampleIdx) / (float)samplesPerCtlPt;
      assert(t_segment >= 0.f && t_segment <= 1.f);
      const float t_strand = (float)sampleIdx / (float)(numCurveSamples - 1);
      assert(t_strand >= 0.f && t_strand <= 1.f);


      // interpolate width linearly TODO:read width control points & interpolate as cubic
      const float width = t_strand * curve->width1 + (1.f - t_strand) * curve->width0;
      assert(width > 0.f && width < 2.f);

      bezier_eval(t_segment, bez_P, skelP, skelT);

      // get the binormal
      B = normalize(cross(N, skelT));
      assert(length(B) > 0.f);
      // mesh verts are a pair of skel pointf offset by the binormal
      meshVerts.push_back(xfmPoint(xfm, skelP - N * 0.5f*width));
      meshVerts.push_back(xfmPoint(xfm, skelP + N * 0.5f*width));
      assert(meshVerts.size() <= numMeshVerts);
    }

    for (int vertIdx = 0; vertIdx < numMeshVerts-2; vertIdx += 2) {
      assert(vertIdx+3 < meshVerts.size());
      mesh->index.push_back(pbrt::math::vec3i(vertIdx+0, vertIdx+1, vertIdx+3));
      mesh->index.push_back(pbrt::math::vec3i(vertIdx+0, vertIdx+2, vertIdx+3));
    }
    assert(mesh->index.size() == numMeshTris);

    // const vec3f v00 = xfmPoint(xfm,P0 - .5f*curve->width0*N);
    // const vec3f v01 = xfmPoint(xfm,P0 + .5f*curve->width0*N);
    // const vec3f v10 = xfmPoint(xfm,P1 - .5f*curve->width1*N);
    // const vec3f v11 = xfmPoint(xfm,P1 + .5f*curve->width1*N);
    
    // mesh->vertex.resize(4);
    // (vec3f&)mesh->vertex[0] = v00;
    // (vec3f&)mesh->vertex[1] = v01;
    // (vec3f&)mesh->vertex[2] = v10;
    // (vec3f&)mesh->vertex[3] = v11;
    // // PRINT(v00);
    // // PRINT(v01);
    // // PRINT(v10);
    // // PRINT(v11);

#if 0
    const vec3f n00 = xfmNormal(xfm,normalize(cross(N,L)));
    const vec3f n01 = n00;
    const vec3f n10 = n00;
    const vec3f n11 = n00;
    mesh->normal.resize(4);
    (vec3f&)mesh->normal[0] = n00;
    (vec3f&)mesh->normal[1] = n01;
    (vec3f&)mesh->normal[2] = n10;
    (vec3f&)mesh->normal[3] = n11;
#endif
    
    // mesh->index.resize(2);
    // (vec3i&)mesh->index[0] = vec3i(0,1,3);
    // (vec3i&)mesh->index[1] = vec3i(0,2,3);

    return mesh;
  }

