// ======================================================================================
//  Copyright (c) 2019 NVIDIA Corporation.  All rights reserved.
//
//  NVIDIA Corporation and its licensors retain all intellectual property and proprietary
//  rights in and to this software, related documentation and any modifications thereto.
//  Any use, reproduction, disclosure or distribution of this software and related
//  documentation without an express license agreement from NVIDIA Corporation is strictly
//  prohibited.
//
//  TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, THIS SOFTWARE IS PROVIDED *AS IS*
//  AND NVIDIA AND ITS SUPPLIERS DISCLAIM ALL WARRANTIES, EITHER EXPRESS OR IMPLIED,
//  INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
//  PARTICULAR PURPOSE.  IN NO EVENT SHALL NVIDIA OR ITS SUPPLIERS BE LIABLE FOR ANY
//  SPECIAL, INCIDENTAL, INDIRECT, OR CONSEQUENTIAL DAMAGES WHATSOEVER (INCLUDING, WITHOUT
//  LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS INTERRUPTION, LOSS OF
//  BUSINESS INFORMATION, OR ANY OTHER PECUNIARY LOSS) ARISING OUT OF THE USE OF OR
//  INABILITY TO USE THIS SOFTWARE, EVEN IF NVIDIA HAS BEEN ADVISED OF THE POSSIBILITY OF
//  SUCH DAMAGES
// ======================================================================================

#include "Renderer.h"
#include "OpenEXR/ImfArray.h"
#include "OpenEXR/ImfRgba.h"
#include "OpenEXR/ImfInputFile.h"
#include "OpenEXR/ImfRgbaFile.h"
#include "OpenEXR/ImathBox.h"

namespace app {

  using namespace Imf;
  using namespace Imath;
  
  int readEXR(Renderer &renderer,
               const std::string fileName)
  {
    Array2D<Rgba> pixels;
    int width;
    int height;

    RgbaInputFile file(fileName.c_str());
    Box2i dw = file.dataWindow();
    width  = dw.max.x - dw.min.x + 1;
    height = dw.max.y - dw.min.y + 1;
    pixels.resizeErase (height, width);
    file.setFrameBuffer (&pixels[0][0] - dw.min.x - dw.min.y * width, 1, width);
    file.readPixels (dw.min.y, dw.max.y);

    std::vector<vec3f> as3f;
    for (int iy=0;iy<height;iy++)
      for (int ix=0;ix<width;ix++) {
        auto &pixel = pixels[iy][ix];
        as3f.push_back(vec3f(pixel.r,pixel.g,pixel.b));
      }

    PRINT(as3f[0]);
    
    auto buffer = renderer.createDeviceBuffer(sizeof(vec3f));
    buffer->upload(vec2i(width,height),as3f);
    return buffer->getID();
  }
  
  int Renderer::loadImageMapIntoBuffer(const std::string &mapName)
  {
    static std::mutex mutex;
    static std::map<std::string,int> alreadyLoaded;
    std::lock_guard<std::mutex> lock(mutex);
    if (alreadyLoaded.find(mapName) == alreadyLoaded.end()) 
      alreadyLoaded[mapName] = readEXR(*this,mapName);
      
    return alreadyLoaded[mapName];
  }

} // ::app
