// ======================================================================== //
// Copyright 2018-2019 Ingo Wald                                            //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#include "pbrtParser/Scene.h"
// std
#include <fstream>
#include <sstream>
#include <set>
#include <deque>
#include <queue>
#include <stdio.h>
#include <iostream>
#include <stdexcept>
#include <memory>
#include <assert.h>
#include <string>
#include <math.h>
#include <cmath>
#include <algorithm>
#include <sys/time.h>
// ptex
#include "Ptexture.h"
// tbb
// #include <tbb/parallel_for.h>
// #include <tbb/task_arena.h>
// #include <tbb/task_scheduler_init.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "owl/common/math/AffineSpace.h"
#include "owl/common/parallel/parallel_for.h"

#ifndef PRINT
# define PRINT(var) std::cout << #var << "=" << var << std::endl;
# define PING std::cout << __FILE__ << "::" << __LINE__ << ": " << __PRETTY_FUNCTION__ << std::endl;
#endif


namespace mowlana {

  using namespace owl;
  using namespace owl::common;
  using pbrt::Scene;
  using pbrt::TriangleMesh;
  using pbrt::QuadMesh;
  using pbrt::PtexFileTexture;
  
  void makeDirs(std::string fileName, bool rec=false)
  {
    size_t rs = fileName.rfind('/');
    if (rs != fileName.npos)
      makeDirs(fileName.substr(0,rs),true);

    if (rec) {
      mkdir(fileName.c_str(),0777);
    }
  }
  
  inline bool endsWith(const std::string &s, const std::string &suffix)
  {
    return s.substr(s.size()-suffix.size(),suffix.size()) == suffix;
  }


#ifdef __WIN32__
#  define osp_snprintf sprintf_s
#else
#  define osp_snprintf snprintf
#endif
  
  
#define LOCK(mtx) std::lock_guard<std::mutex> _lock(mtx)

  inline uint32_t make_8bit(const float f)
  {
    return std::min(255,std::max(0,int(f*256.f)));
  }

  inline uint32_t toRGBA8(const vec3f color)
  {
    return
      (make_8bit(color.x) << 0) +
      (make_8bit(color.y) << 8) +
      (make_8bit(color.z) << 16) +
      (255 << 24);
  }
  
  void bakeOut(const std::string &outFileBase,
               const std::string &inPtexDir,
               const std::string ptexName,
               QuadMesh::SP quads,
               int RES)
  {
    std::string error = "";
    Ptex::PtexTexture *tex = Ptex::PtexTexture::open((inPtexDir+"/"+ptexName).c_str(),error);
    if (error != "")
      throw std::runtime_error("ptex error : "+error);
    Ptex::PtexFilter::Options defaultOptions;
    Ptex::PtexFilter *filter = Ptex::PtexFilter::getFilter(tex,defaultOptions);

    std::cout << " - baking out " << ptexName << std::endl;
    std::vector<uint32_t> texels;
    for (size_t faceID=0;faceID<quads->index.size();faceID++) {
      
      for (int iy=0;iy<RES;iy++)
        for (int ix=0;ix<RES;ix++)
          {
            vec3f color = { 0.f,0.f,0.f };
            // if (bilinear) {
              vec3f texel;
              vec2f uv = vec2f(ix,iy) * (1.f/float(RES-1));
              filter->eval(&texel.x,0,3,faceID,uv.x,uv.y,0,0,0,0);
              
              color = texel;

              // } else {
              // const int numSamples = 16;
              // for (int i=0;i<numSamples;i++) {
              //   vec3f texel;
              //   vec2f uv = vec2f(ix+(float)drand48(),iy+(float)drand48())*(1.f/float(RES));
              //   filter->eval(&texel.x,0,3,faceID,uv.x,uv.y,0,0,0,0);
              //   color = color + texel;
              //      }
              // color = color * (1.f/float(numSamples));
            // }
            texels.push_back(toRGBA8(color));
          }
    }
    
    std::stringstream outFileName;
    outFileName << outFileBase << "/";
    // for (auto c : ptexName) {
    //   if (c == '/' || c == '.' || c == ':' || c == '\\')
    //     outFileName << '_';
    //   else
    //     outFileName << c;
    // }
    outFileName << ptexName;
    outFileName << ".bpt";

    makeDirs(outFileName.str());
    
    std::ofstream out(outFileName.str());
    int numPrims = quads->index.size();
    out.write((const char*)&numPrims,sizeof(numPrims));
    out.write((const char*)&RES,sizeof(RES));
    out.write((const char*)texels.data(),texels.size()*sizeof(texels[0]));
    std::cout << "\t-> " << outFileName.str() << std::endl;
    filter->release();
    tex->release();
  }


  void usage(const std::string &msg)
  {
    if (!msg.empty()) std::cerr << std::endl << "***Error***: " << msg << std::endl << std::endl;
    std::cout << "Usage: ./pbrtPTexBaker inFile.pbf ptexFilesRootDir -o outPathPrefix <args>" << std::endl;
    std::cout << "Args:" << std::endl;
    std::cout << " -res <resolution> : resolution to bake out with" << std::endl;
    std::cout << " -o outPathPrefix   : base string for all generated file names (required)" << std::endl;
    std::cout << "ptexFilesRootDir is the directory that all ptex files names will be considered relative to" << std::endl;
    exit(msg != "");
  }

  extern "C" int main(int ac, char **av)
  {
    std::string inFileName = "";
    std::string inPtexDir  = "";
    std::string outFileBase = "";
    int res = 16;
    // bool bilinear;
      
    for (int i=1;i<ac;i++) {
      const std::string arg = av[i];
      if (arg == "-o") {
        outFileBase = av[++i];
      } else if (arg == "-res") {
        res = atoi(av[++i]);
      } else if (arg[0] != '-') {
        if (inFileName == "")
          inFileName = arg;
        else if (inPtexDir == "")
          inPtexDir = arg;
        else
          usage("both pbf file and ptex directory already specified!?");
      } else
        usage("unknown cmd line arg '"+arg+"'");
    }

    if (inFileName == "") usage("no input file name specified");
    if (inPtexDir == "") usage("no ptex directory location specified");
    if (outFileBase == "") usage("no output file name base specified");

    std::cout << "==================================================================" << std::endl;
    std::cout << "loading scene..." << std::endl;
    std::cout << "==================================================================" << std::endl;

    Scene::SP inScene = Scene::loadFrom(inFileName);
    inScene->makeSingleLevel();

    std::cout << "==================================================================" << std::endl;
    std::cout << "precomputing global stuff..." << std::endl;
    std::cout << "==================================================================" << std::endl;
      
    std::set<TriangleMesh::SP> activeMeshes;
    for (auto inst : inScene->world->instances)
      for (auto geom : inst->object->shapes) {
        TriangleMesh::SP mesh = std::dynamic_pointer_cast<TriangleMesh>(geom);
        if (!mesh) continue;
        activeMeshes.insert(mesh);
      }
    std::cout << "found a total of " << activeMeshes.size() << " active meshes" << std::endl;

    
    std::cout << "==================================================================" << std::endl;
    std::cout << "extracting per-quad textures..." << std::endl;
    std::cout << "==================================================================" << std::endl;
    
    std::map<std::string,QuadMesh::SP> activePtexs;
    for (auto triMesh : activeMeshes) {
      QuadMesh::SP quads = QuadMesh::makeFrom(triMesh);
      for (auto texture : quads->textures) {
        PtexFileTexture::SP ptex = std::dynamic_pointer_cast<PtexFileTexture>(texture.second);
        if (!ptex)
          continue;

        if (!endsWith(ptex->fileName,".ptex") &&
            !endsWith(ptex->fileName,".ptx")) {
          throw std::runtime_error("ptex texture, but does not end in .ptex or .ptx??? (fileName="+ptex->fileName+")");
          continue;
        }
        
        if (texture.first == "color") {
          activePtexs[ptex->fileName] = quads;
        } else if (texture.first == "bumpmap") {
          std::cout << "ignoring 'bumpmap' (that's supposed to be a displacement, anyway ..." << std::endl;
        } else
          throw std::runtime_error("unknown ptex texture type "+texture.first);
      }
    }

    size_t totalTexturedQuads = 0;
    for (auto active : activePtexs) {
      std::cout << active.first << " : on mesh w/ "
                << active.second->getNumPrims() << " quads" << std::endl;
      totalTexturedQuads += active.second->getNumPrims();
    }
    std::cout << "found a total of " << activePtexs.size() << " 'color'-mapped ptex textures, "
              << "on a total of " << prettyNumber(totalTexturedQuads) << " quads ..." << std::endl;

#if 1
    {
      std::vector<std::string> fileName;
      std::vector<QuadMesh::SP> quadMesh;
      for (auto active : activePtexs) {
        fileName.push_back(active.first);
        quadMesh.push_back(active.second);
      }
      parallel_for(fileName.size(),[&](int ptexID){
          bakeOut(outFileBase,inPtexDir,fileName[ptexID],quadMesh[ptexID],res);
        });
    }
#else
    for (auto active : activePtexs) {
      bakeOut(outFileBase,active.first,active.second,res);
    }
#endif
    
    std::cout << std::endl;
    std::cout << "==================================================================" << std::endl;
    std::cout << "done" << std::endl;
    std::cout << "==================================================================" << std::endl;
    
  }

} // ::pbrt
