# Mowlana - Moana on OWL...

owl-based viewer of pbrt-files (in particular, moana-related) ... work in progress.

# Data Preprocessing

Moana comes in two possible formats, either PBRT's `.pbrt` or
Wavefront's `.obj` format (and of course, some native `.json` files
that are non-trivial to parse). Since all three format take a few
lifetimes to pass, Mowlana only supports out `pbrtParser` library's
'binary pbrt format' (`.pbf`), which can read the final file in a few
seconds. Also, we'll need to bake out textures.

To do this steps:

## First, build the source code 

Follow instruction below, until a `make` will build the project.

## Second, convert the PRBT version to pbf

The default `make` rule will only build the viewer, not the
pbrtLibrary's `pbrt2pbf` converter. So, let's build this explicitly:

	make pbrt2pbf
	
Then, convert:

	mkdir ~/models/island
	pbrt2pbf ~/downloaded/island/pbrt/island.pbrt -o ~/models/island/island.pbf
	
You can check the result of this via

	pbfInfo ~/models/island/island.pbf

This obviously assumes you downloaded and unpacked to
`~/downloaded/island` ... pls adjust accordingly.

## Third, bake out the PTex textures

Disney uses PTex as a texture format; but there's no native PTex
library for CUDA (well, not that I could find any). So, we instead
decided to bake those textures out into a texture atlas, for which we
developed a shiny new tool (which is actually stolen from a branch
in said pbrtParser library, if you want to use it w/o mowlana).

First, build this baking tool:

	make pbrtPtexBaker
	
Once built, all you need to do is run it as such:

	./pbrtPtexBaker                   \
		~/models/island/island.pbf    \
		~/downloaded/island/textures/ \
		-o ~/models/island/baked-16x16  \
		-res 16

The first of those parameters is the `pbf` file we already created;
the second one the directory of Ptex textures that the origianl model
comes with.

Of course, you can also use 4x4, 8x8, or 32x32.... with an obvious
tradeoff of quality vs size (though 8x8 is usually OK).

This should take about a minute or so, and produce a lot of `.ptx.bpt`
files (bpt for baked ptex texture; each one is a texture atlas of lots of little
XnX textures).

## Finally: Render

Once model is converted and textures are baked, you can render as such:

	./mowlanaViewer                       \
		~/models/island/island.pbf        \
		-t ~/models/island/baked-16x16/
	
# command line options

- `-spp <numSamplesPerPixel>` num samples per pixel
- `-win x y` window size
- `--measure-time <numSecs>` measure for s secs, then print frames per sec and exit
- `--camera <from> <at> <up>` set camera position, look-at, and up-vector (all float3)
- `-t path` set path to directory containing the textures
- `--max-num-bounces <mnb>` number of bounces in path tracer (0 =
  primary rays, 1 = ambient occlusoin equivalent, etc)

# key strokes

- `C` print camera position
- `0`-`9` set shade mode
- `x,y,z` switch camera upvector to x,y,z (pressing twice flip up/down)
- `!` screenshot

# cmake build options

- `MEASURE_ANYHIT_CALLS` : if on, will print num anyhit progs at end
  of frame, *and* will make heatmap show num anyhit calls instead of
  cycles

- `FORCE_DISABLE_ANYHIT` : disable all anyhit programs

- `FORCE_OPAQUE_ANYHIT` : anyhit executes, but is ALWAYS opaque, no
  matter what the texture says





