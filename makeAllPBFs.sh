#!/bin/bash
make pbrt2pbf
make pbrtPtexBaker
for f in pandanus naupaka mountain isCoral bayCedar island; do
    echo ###################### making $f ######################    
    ./pbrt2pbf ~/models/moana/pbrt/$f.pbrt -o ~/models/island/$f.pbf
    ./pbrtPtexBaker ~/models/island/$f.pbf ~/models/moana/textures/ -o ~/models/island/baked-16x16/ -res 16

done
