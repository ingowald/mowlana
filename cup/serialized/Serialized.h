// ======================================================================== //
// Copyright 2019-2020 Ingo Wald                                            //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#pragma once

#include "cup/cup.h"
#include <vector>
#include <map>
#include <set>

namespace cup {

  /*! tool that helps serializing objects by offering a "push(obj)"
      method that will insert the given object into a linear list (if
      not already in there), and reutrn whether this object was new or
      not; and by offering a getIdx(object) method that reutrns the
      linear index of the given object (if known), or -1, if not */
  template<typename T>
  struct Serialized {
    
    typedef enum { ALREADY_KNOWN, THIS_WAS_NEW } Result;

    inline size_t   size() const;
    inline const T *data() const;
    inline T        operator[](size_t idx) const;
    
    inline int      getIdx(const T &t) const;
    inline Result   add(const T &t);
    
    std::vector<T>  asList;
    std::map<T,int> index;
  };

  /*! for textures, we want to make sure that we properly detect
      multiple uses of the same file name even if that is used for
      different 'class' instances of a pbrt::...Texture class */
  struct SerializedTextures : public Serialized<pbrt::Texture::SP>
  {
    inline Result   add(const pbrt::Texture::SP &t)
    {
      if (!t) return ALREADY_KNOWN;
      
      if (index.find(t) != index.end())
        // already know this one
        return ALREADY_KNOWN;

      if (pbrt::PtexFileTexture::SP image = t->as<pbrt::PtexFileTexture>()) {
        auto it = firstInstanceOfPtexFileTexture.find(image->fileName);
        if (it != firstInstanceOfPtexFileTexture.end()) {
          index[t] = index[it->second];
          return THIS_WAS_NEW;
        }
        int idx = asList.size();
        asList.push_back(t);
        firstInstanceOfPtexFileTexture[image->fileName] = t;
        index[t] = idx;
        return ALREADY_KNOWN; //THIS_WAS_NEW;
      }

      if (pbrt::ImageTexture::SP image = t->as<pbrt::ImageTexture>()) {
        auto it = firstInstanceOfImageTexture.find(image->fileName);
        if (it != firstInstanceOfImageTexture.end()) {
          index[t] = index[it->second];
          return THIS_WAS_NEW;
        }
        int idx = asList.size();
        asList.push_back(t);
        firstInstanceOfImageTexture[image->fileName] = t;
        index[t] = idx;
        return ALREADY_KNOWN; //THIS_WAS_NEW;
      }
      
      int idx = asList.size();
      asList.push_back(t);
      index[t] = idx;
      return THIS_WAS_NEW;
    }
    
    std::map<std::string,pbrt::Texture::SP> firstInstanceOfImageTexture;
    std::map<std::string,pbrt::Texture::SP> firstInstanceOfPtexFileTexture;
  };
  

  template<typename T>
  size_t Serialized<T>::size() const
  { return asList.size(); }
  
  template<typename T>
  const T *Serialized<T>::data() const
  { return asList.data(); }
  
  template<typename T>
  T Serialized<T>::operator[](size_t idx) const
  { return asList[idx]; }
    
  template<typename T>
  int Serialized<T>::getIdx(const T &t) const
  {
    if (!t) return -1;
    auto it = index.find(t);
    return it == index.end() ? -1 : it->second;
  }

  template<typename T>
  typename Serialized<T>::Result Serialized<T>::add(const T &t)
  {
    if (!t) return ALREADY_KNOWN;
      
    if (index.find(t) != index.end())
      // already know this one
      return ALREADY_KNOWN;
    index[t] = (int)asList.size();
    asList.push_back(t);
    // notifyThatThisIsNew();
    return THIS_WAS_NEW;
  }
  
}

