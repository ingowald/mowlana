// ======================================================================== //
// Copyright 2019-2020 Ingo Wald                                            //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#include "cup/serialized/Serializer.h"

namespace cup {
  namespace serialized {
    
    Serializer::Serializer(Scene *target, pbrt::Scene::SP scene)
      : target(target)
    {
      std::cout << "#cup.serialized: "
                << "serializing scene ..." << std::endl;
      for (auto inst : scene->world->instances)
        if (inst && inst->object)
          serialize(inst->object);
      std::cout << "#cup.serialized: "
                << "done serializing scene." << std::endl;
    }

    void Serializer::serialize(pbrt::Object::SP   object)
    {
      if (!object || target->objects.add(object) == target->objects.ALREADY_KNOWN)
        return;

      assert(object->instances.empty());
      for (auto shape : object->shapes)
        serialize(shape);
    }
    
  } // ::cup::serialized
} // ::cup
