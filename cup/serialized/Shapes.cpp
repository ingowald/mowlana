// ======================================================================== //
// Copyright 2019-2020 Ingo Wald                                            //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#include "cup/serialized/Serializer.h"

namespace cup {
  namespace serialized {
    
    void Serializer::serialize(pbrt::Shape::SP shape)
    {
      if (!shape || target->shapes.add(shape) == target->shapes.ALREADY_KNOWN)
        return;
    
      // serialize this shape's material
      serialize(shape->material);
    
      // ... and now, it's textures, if it has any
      for (auto texture : shape->textures) {
        if (texture.first == "color")//PRINT(texture.first);
          serialize(texture.second);
      }
    }

  } // ::cup::serialized
} // ::cup
