// ======================================================================== //
// Copyright 2019-2020 Ingo Wald                                            //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#include "cup/serialized/Serializer.h"

namespace cup {
  namespace serialized {
    
    void Serializer::serialize(pbrt::Material::SP material)
    {
      if (!material || target->materials.add(material) == target->materials.ALREADY_KNOWN)
        return;
    
      if (auto translucent = material->as<pbrt::TranslucentMaterial>()) {
        serialize(translucent->map_kd);
        return;
      }
      if (auto glass = material->as<pbrt::GlassMaterial>()) {
        return;
      }
      if (auto mirror = material->as<pbrt::MirrorMaterial>()) {
        return;
      }
      if (auto mix = material->as<pbrt::MixMaterial>()) {
        serialize(mix->material0);
        serialize(mix->material1);
        serialize(mix->map_amount);
        return;
      }
      if (auto plastic = material->as<pbrt::PlasticMaterial>()) {
        serialize(plastic->map_kd);
        serialize(plastic->map_ks);
        serialize(plastic->map_bump);
        serialize(plastic->map_roughness);
        return;
      }
      if (auto matte = material->as<pbrt::MatteMaterial>()) {
        serialize(matte->map_kd);
        serialize(matte->map_sigma);
        serialize(matte->map_bump);
        return;
      }
      if (auto disney = material->as<pbrt::DisneyMaterial>()) {
        return;
      }
      if (auto metal = material->as<pbrt::MetalMaterial>()) {
        return;
      }
      if (auto uber = material->as<pbrt::UberMaterial>()) {
        serialize(uber->map_kd);
        serialize(uber->map_ks);
        serialize(uber->map_kr);
        serialize(uber->map_kt);
        serialize(uber->map_opacity);
        serialize(uber->map_alpha);
        serialize(uber->map_shadowAlpha);
        serialize(uber->map_uRoughness);
        serialize(uber->map_vRoughness);
        serialize(uber->map_roughness);
        serialize(uber->map_bump);
        return;
      }

      { /* emit (one!) warning if we see a type that's not yet handled by cup */
        static std::set<std::string> alreadyWarned;
        static std::mutex mutex;
        const std::string type = material->toString();
        std::lock_guard<std::mutex> lock(mutex);
        if (alreadyWarned.find(type) == alreadyWarned.end()) {
          std::cout << OWL_TERMINAL_LIGHT_RED
                    << "WARNING: un-recognized material '"
                    << type << "'"
                    << OWL_TERMINAL_DEFAULT
                    << std::endl;
          alreadyWarned.insert(type);
        }
      }
    }

  } // ::cup::serialized
} // ::cup
