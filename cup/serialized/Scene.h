// ======================================================================== //
// Copyright 2019-2020 Ingo Wald                                            //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#pragma once

#include "Serialized.h"

namespace cup {
  namespace serialized {

    using AreaLightsList = std::vector<std::pair<pbrt::Instance::SP,pbrt::Shape::SP>>;
      
    /* note instances do not get serialized because we assume
       single-level scene */
    struct Scene {
      typedef std::shared_ptr<Scene> SP;
      
      static const uint64_t TESSELLATE_CURVES      = (1<<0);
      static const uint64_t REMOVE_NON_MESH_SHAPES = (1<<1);
    
      Scene(pbrt::Scene::SP scene);

      static Scene::SP load(const std::string &fileName,
                            uint64_t flags = TESSELLATE_CURVES|REMOVE_NON_MESH_SHAPES);
      
      box3f getWorldBounds(bool excludeLights=true) const;

      inline int getIdx(const pbrt::Shape::SP &t)    const;
      inline int getIdx(const pbrt::Material::SP &t) const;
      inline int getIdx(const pbrt::Object::SP &t)   const;
      inline int getIdx(const pbrt::Texture::SP  &t) const;
      
      Serialized<pbrt::Material::SP> materials;
      Serialized<pbrt::Shape::SP>    shapes;
      Serialized<pbrt::Object::SP>   objects;
      SerializedTextures  textures;

      AreaLightsList areaLights;
      std::vector<pbrt::LightSource::SP> globalLights;
      
      pbrt::Scene::SP pbrtScene;
    };


    inline int Scene::getIdx(const pbrt::Shape::SP &t)    const { return shapes.getIdx(t); }
    inline int Scene::getIdx(const pbrt::Material::SP &t) const { return materials.getIdx(t); }
    inline int Scene::getIdx(const pbrt::Object::SP &t)   const { return objects.getIdx(t); }
    inline int Scene::getIdx(const pbrt::Texture::SP  &t) const { return textures.getIdx(t); }

  } // ::cup::serialized
} // ::cup
