// ======================================================================== //
// Copyright 2019-2020 Ingo Wald                                            //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#include "cup/serialized/Serializer.h"
#include "cup/tools/tessellateCurves.h"
#include "cup/tools/removeAllNonMeshShapes.h"
#include "cup/tools/removeAllNonMeshShapes.h"
#include "cup/tools/Lights.h"

namespace cup {
  namespace serialized {
    
    Scene::SP Scene::load(const std::string &fileName,
                          uint64_t flags)
    {
      std::cout << "#cup.serialized: "
                << "reading pbf from " << fileName << std::endl;
      
      pbrt::Scene::SP pbrtScene
        = pbrt::Scene::loadFrom(fileName);
      assert(pbrtScene);
      std::cout << "#cup.serialized: "
                << "pbf file read, converting to single-level scene ..." << std::endl;
      pbrtScene->makeSingleLevel();
      
      if (flags & TESSELLATE_CURVES) {
        std::cout << "#cup.serialized: "
                  << "tessellating curves ..." << std::endl;
        tools::tessellateAllCurvesIn(pbrtScene);
      }
      if (flags & REMOVE_NON_MESH_SHAPES) {
        std::cout << "#cup.serialized: "
                  << "removing all non-mesh shapes ..." << std::endl;
        tools::removeAllNonMeshShapes(pbrtScene);
      }
      std::cout << "#cup.serialized: "
                << "scene imported ..." << std::endl;
      return std::make_shared<serialized::Scene>(pbrtScene);
    }
      

    /*! kick out all shapes that are tagged as area lights */
    AreaLightsList separateAreaLights(pbrt::Scene::SP scene)
    {
      AreaLightsList lights;
      for (auto inst : scene->world->instances) 
        if (inst && inst->object)
          for (auto shape : inst->object->shapes)
            if (shape && shape->areaLight)
              lights.push_back({inst,shape});
      
      std::set<pbrt::Object::SP> objects;
      for (auto inst : scene->world->instances)
        if (inst && inst->object)
          objects.insert(inst->object);

      for (auto object : objects) {
        std::vector<pbrt::Shape::SP> noLightShapes;
        for (auto shape : object->shapes)
          if (shape && !shape->areaLight)
            noLightShapes.push_back(shape);
        object->shapes = noLightShapes;
        object->haveComputedBounds = false;
      }

      std::vector<pbrt::Instance::SP> remainingInstances;
      for (auto inst : scene->world->instances) {
        if (!inst) {
          remainingInstances.push_back(inst);
          continue;
        }
        if (inst && inst->object && !inst->object->shapes.empty()) {
          remainingInstances.push_back(inst);
          inst->haveComputedBounds = false;
        }
      }
      scene->world->instances = remainingInstances;
      scene->world->haveComputedBounds = false;
      return lights;
    }
    
    
    Scene::Scene(pbrt::Scene::SP scene)
      : pbrtScene(scene)
    {
      assert(scene);

      PING;
      globalLights = scene->world->lightSources;
      PRINT(globalLights.size());
      
      if (!scene->isSingleLevel())
        scene->makeSingleLevel();

      Serializer serializer(this,scene);

      areaLights = separateAreaLights(scene);
      PRINT(areaLights.size());
    }

  } // ::cup::serialized
} // ::cup
