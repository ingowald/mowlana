// ======================================================================== //
// Copyright 2019-2020 Ingo Wald                                            //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#include "cup/serialized/Serializer.h"

namespace cup {
  namespace serialized {
    
    void Serializer::serialize(pbrt::Texture::SP texture)
    {
      if (!texture || target->textures.add(texture) == target->textures.ALREADY_KNOWN)
        return;

      if (auto image = texture->as<pbrt::ImageTexture>()) {
        /* nothing to do */
        return;
      }
      if (auto image = texture->as<pbrt::PtexFileTexture>()) {
        /* nothing to do */
        return;
      }
      if (auto image = texture->as<pbrt::ConstantTexture>()) {
        /* nothing to do */
        return;
      }

      { /* emit (one!) warning if we see a type that's not yet handled by cup */
        static std::set<std::string> alreadyWarned;
        static std::mutex mutex;
        const std::string type = texture->toString();
        std::lock_guard<std::mutex> lock(mutex);
        if (alreadyWarned.find(type) == alreadyWarned.end()) {
          std::cout << OWL_TERMINAL_LIGHT_RED
                    << "WARNING: un-recognized texture '"
                    << type << "' (more instances may follow)"
                    << OWL_TERMINAL_DEFAULT
                    << std::endl;
          alreadyWarned.insert(type);
        }
      }
    }

  } // ::cup::serialized
} // ::cup
