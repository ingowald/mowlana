// ======================================================================== //
// Copyright 2019-2020 Ingo Wald                                            //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#include "serialized/Scene.h"

int main(int ac, char **av)
{
  if (ac != 2)
    throw std::runtime_error("usage: ./cupInfo input.pbf");
  
  cup::serialized::Scene::SP serialized = cup::serialized::Scene::load(av[1]);
  
  PRINT(serialized->materials.size());
  PRINT(serialized->textures.size());
  PRINT(serialized->objects.size());
  PRINT(serialized->shapes.size());
  
}
