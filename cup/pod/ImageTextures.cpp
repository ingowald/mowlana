// ======================================================================== //
// Copyright 2019-2020 Ingo Wald                                            //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#include "cup/pod/ImageTextures.h"
#include <samples/common/3rdParty/stb/stb_image.h>
#include <samples/common/3rdParty/stb/stb_image_write.h>
#include "owl/common/parallel/parallel_for.h"
#include <owl/helper/cuda.h>
#include <fstream>

namespace cup {
  namespace pod {

    ImageTextureHost::~ImageTextureHost()
    {
      if (texels) {
        stbi_image_free(texels); texels = nullptr;
      }
    }
    
    void loadImageTexture(ImageTextureHost &image,
                          pbrt::ImageTexture::SP pbrt,
                          const std::string &searchPath)
    {
      const std::string fileName = searchPath+"/"+pbrt->fileName;
    
      image.texels = stbi_load(fileName.c_str(),
                               &image.size.x,
                               &image.size.y,
                               &image.numChannels,
                               STBI_rgb_alpha);
      if (!image.texels) {
        std::cout << OWL_TERMINAL_RED
                  << "Warning: could not read texture '"
                  << fileName
                  << "'"
                  << OWL_TERMINAL_DEFAULT
                  << std::endl;
        return;
      }
      std::cout << OWL_TERMINAL_LIGHT_GREEN
                << "successfully loaded texture '"
                << fileName
                << "'"
                << OWL_TERMINAL_DEFAULT
                << std::endl;
    }
  
    void loadBakedPtex(ImageTextureHost &image,
                       pbrt::PtexFileTexture::SP pbrt,
                       const std::string &searchPath)
    {
      const std::string fileName = searchPath+"/"+pbrt->fileName+".bpt";

      std::ifstream in(fileName,std::ios::binary);
      if (in.good()) {
        in.read((char*)&image.numPrims,sizeof(image.numPrims));
        in.read((char*)&image.bakeRes,sizeof(image.bakeRes));
        int res = image.bakeRes;
        
        int blocks_x = BAKING_ATLAS_WIDTH/res;
        int blocks_y = divRoundUp(image.numPrims,blocks_x);

        image.size.x = blocks_x * res;
        image.size.y = blocks_y * res;
        std::vector<uint32_t> tmp(image.numPrims*image.bakeRes*image.bakeRes);
                                  //blocks_x*blocks_y*image.bakeRes*image.bakeRes);
        
        in.read((char*)tmp.data(),image.numPrims*image.bakeRes*image.bakeRes*sizeof(uint32_t));

        image.texels = (unsigned char*)(new uint32_t[image.size.x*image.size.y]);//blocks_x*blocks_y*image.bakeRes*image.bakeRes]);
        memset(image.texels,0,image.size.x*image.size.y*sizeof(uint32_t));
        std::cout << "ptex " << fileName << " -> " << image.size << std::endl;
        parallel_for
          (image.numPrims,
           [&](int primID){//for (int primID=0;primID<image.numPrims;primID++) {
             int bx = primID % blocks_x;
             int by = primID / blocks_x;
             for (int iy=0;iy<res;iy++)
               for (int ix=0;ix<res;ix++) {
                 int lidx = primID*res*res + res*iy + ix;
                 int iix = res*bx+ix;
                 int iiy = res*by+iy;
                 ((uint32_t*)image.texels)[iix+iiy*image.size.x] = tmp[lidx];
               }
           });
        image.isBakedPtex = true;
        image.bakeRes = res;

#if 0
        static int _dumpID = 0;
        int dumpID = _dumpID++;
        std::string fileName = "dump_atlas_"+std::to_string(dumpID)+".png";
        stbi_write_png(fileName.c_str(),image.size.x,image.size.y,4,
                       image.texels,image.size.x*sizeof(uint32_t));
        PRINT(fileName);
#endif
      }
      if (!image.texels) {
        std::cout << OWL_TERMINAL_RED
                  << "Warning: could not read texture '"
                  << fileName
                  << "'"
                  << OWL_TERMINAL_DEFAULT
                  << std::endl;
        return;
      }
      std::cout << OWL_TERMINAL_LIGHT_GREEN
                << "successfully loaded texture '"
                << fileName
                << "'"
                << OWL_TERMINAL_DEFAULT
                << std::endl;
    }
  
    
    /*! tries to load all textures in the serialized scene as image
      textures; the result vector will alwys be the same size as
      scene->textures, but those textures that are either not image
      textures or that cannot be loaded will be set to a
      pos::ImageTexture with 0 size and null texels pointer */ 
    void loadImageTextures(std::vector<ImageTextureHost> &results,
                           const cup::serialized::Scene::SP &scene,
                           const std::string &searchPath)
    {
      results.resize(scene->textures.size());
      // for (int i=0;i<textures.size();i++) {1
      owl::parallel_for(scene->textures.size(),[&](int i) {
          pbrt::Texture::SP tex = scene->textures[i];
          if (pbrt::ImageTexture::SP img = tex->as<pbrt::ImageTexture>())
            loadImageTexture(results[i],img,searchPath);
          else if (pbrt::PtexFileTexture::SP img = tex->as<pbrt::PtexFileTexture>())
            loadBakedPtex(results[i],img,searchPath);
        });
    }
    
    /*! given the currently active GPU, create a list of cuda texture
      objects for the given vector of host textures */
    void createTextureObjects(/*! whether the resulting texture is a
                                  baked ptex texture, which requires
                                  different texture coordinate
                                  generation */
                              std::vector<cudaArray_t> &resultArrays,
                              std::vector<cudaTextureObject_t> &resultTextures,
                              const std::vector<ImageTextureHost> &images)
    {
      resultArrays.resize(images.size());
      resultTextures.resize(images.size());
      memset(resultArrays.data(),0,resultArrays.size()*sizeof(resultArrays[0]));
      memset(resultTextures.data(),0,resultTextures.size()*sizeof(resultTextures[0]));

      for (int i=0;i<images.size();i++) {
        cudaResourceDesc res_desc = {};

        auto &image = images[i];
        
        if (!image.texels)
          continue;

        if (image.isBakedPtex) {
          int numBlocksX = BAKING_ATLAS_WIDTH/image.bakeRes;
          int32_t width  = BAKING_ATLAS_WIDTH;
          int32_t height = image.bakeRes*divRoundUp(image.numPrims,numBlocksX);
          int32_t pitch  = width*4*sizeof(uint8_t);

          cudaChannelFormatDesc channel_desc
            = cudaCreateChannelDesc<uchar4>();
        
          cudaArray_t   &pixelArray = resultArrays[i];
          CUDA_CALL(MallocArray(&pixelArray,
                                &channel_desc,
                                width,height));

          CUDA_CALL(Memcpy2DToArray(pixelArray,
                                    /* offset */0,0,
                                    image.texels,
                                    pitch,pitch,height,
                                    cudaMemcpyHostToDevice));
        
          res_desc.resType          = cudaResourceTypeArray;
          res_desc.res.array.array  = pixelArray;
        
          cudaTextureDesc tex_desc     = {};
          tex_desc.addressMode[0]      = cudaAddressModeWrap;
          tex_desc.addressMode[1]      = cudaAddressModeWrap;
          tex_desc.filterMode          = cudaFilterModeLinear;
          tex_desc.readMode            = cudaReadModeNormalizedFloat;
          tex_desc.normalizedCoords    = 1;
          tex_desc.maxAnisotropy       = 1;
          tex_desc.maxMipmapLevelClamp = 0;//99;
          tex_desc.minMipmapLevelClamp = 0;
          tex_desc.mipmapFilterMode    = cudaFilterModePoint;
          tex_desc.borderColor[0]      = 1.0f;
          tex_desc.sRGB                = 0;

          auto &cuda_tex = resultTextures[i];
          CUDA_CALL(CreateTextureObject(&cuda_tex, &res_desc, &tex_desc, nullptr));
        } else {
          int32_t width  = image.size.x;
          int32_t height = image.size.y;
          int32_t numComponents = image.numChannels;
          int32_t pitch  = width*4*sizeof(uint8_t);
        
          std::vector<uint8_t> uc4(width*height*4);
          const uint8_t *in = image.texels;
          switch(image.numChannels) {
          case 1:
            for (int y=height-1;y>=0;--y) {
              uint8_t *out = uc4.data() + 4*y*width;
              for (int i=0;i<width;i++) {
                int alpha = *in; in += 4;
                *out++ = alpha;
                *out++ = alpha;
                *out++ = alpha;
                *out++ = alpha;
              }
            } break;
          case 3:
          case 4:
            for (int y=height-1;y>=0;--y) {
              uint8_t *out = uc4.data() + 4*y*width;
              for (int i=0;i<width;i++) {
                *out++ = *in++;//255;
                *out++ = *in++;
                *out++ = *in++;
                *out++ = *in++;
              }
            } break;
          default:
            throw std::runtime_error("unsupported num channels in image");
          };
      
          cudaChannelFormatDesc channel_desc
            = cudaCreateChannelDesc<uchar4>();
        
          cudaArray_t   &pixelArray = resultArrays[i];
          CUDA_CALL(MallocArray(&pixelArray,
                                &channel_desc,
                                width,height));
        
          CUDA_CALL(Memcpy2DToArray(pixelArray,
                                    /* offset */0,0,
                                    uc4.data(),//image.texels,
                                    pitch,pitch,height,
                                    cudaMemcpyHostToDevice));
        
          res_desc.resType          = cudaResourceTypeArray;
          res_desc.res.array.array  = pixelArray;
        
          cudaTextureDesc tex_desc     = {};
          tex_desc.addressMode[0]      = cudaAddressModeWrap;
          tex_desc.addressMode[1]      = cudaAddressModeWrap;
          tex_desc.filterMode          = cudaFilterModeLinear;
          tex_desc.readMode            = cudaReadModeNormalizedFloat;
          tex_desc.normalizedCoords    = 1;
          tex_desc.maxAnisotropy       = 1;
          tex_desc.maxMipmapLevelClamp = 0;//99;
          tex_desc.minMipmapLevelClamp = 0;
          tex_desc.mipmapFilterMode    = cudaFilterModePoint;
          tex_desc.borderColor[0]      = 1.0f;
          tex_desc.sRGB                = 0;
      
          auto &cuda_tex = resultTextures[i];
          CUDA_CALL(CreateTextureObject(&cuda_tex, &res_desc, &tex_desc, nullptr));
        }
      }
    }
                      
  } // ::cup::pod
} // ::cup
