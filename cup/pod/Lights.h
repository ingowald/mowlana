// ======================================================================== //
// Copyright 2019-2020 Ingo Wald                                            //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#pragma once

#include "cup/cup.h"

namespace cup {
  namespace pod {
    
    struct SurfaceSample {
      vec3f P;
      vec3f N;
    };

    /*! a light sample that points *from* a surface position *towards*
        the light, that is 'dist' units away, that carries radiance L
        (in reverse directon of dir), and that was sampled with pdf
        'pdf' */
    struct LightSample {
      vec3f dir;
      vec3f L;
      float dist;
      float pdf;
    };



    /*! a *instantiated* quad light - in pbrt, area ligths can live in
      instances, but for ligth sampling you proabbly want to have them
      in a single list of world-space lights.... this is one
      world-sapce instance of a instantiated quad light */
    struct QuadLight {
      vec3f L;
      /*! vertex at (u,v)=(0,0) */
      vec3f P;
      /*! vector pointing along u edge */
      vec3f du;
      /*! vector pointing along v edge */
      vec3f dv;

      float area;
      vec3f N;

      template<typename Random>
      inline __device__ bool sample(LightSample &light,
                                    const SurfaceSample &surface,
                                    Random &random) const;
    };

    template<typename Random>
    inline __device__ bool QuadLight::sample(LightSample &light,
                                             const SurfaceSample &surface,
                                             Random &random) const
    {
      if (//dot(dir,N) > 0.f ||
          area == 0.f)
        return false;

      // PBRT:
      // Spectrum L(const Interaction &intr, const Vector3f &w) const {
      //   return (twoSided || Dot(intr.n, w) > 0) ? Lemit : Spectrum(0.f);
      // }
      const vec3f thisL = this->L;

      // Interaction Triangle::Sample(const Point2f &u, Float *pdf) const {
//     Point2f b = UniformSampleTriangle(u);
//     // Get triangle vertices in _p0_, _p1_, and _p2_
//     const Point3f &p0 = mesh->p[v[0]];
//     const Point3f &p1 = mesh->p[v[1]];
//     const Point3f &p2 = mesh->p[v[2]];
//     Interaction it;
//     it.p = b[0] * p0 + b[1] * p1 + (1 - b[0] - b[1]) * p2;
//     // Compute surface normal for sampled point on triangle
//     it.n = Normalize(Normal3f(Cross(p1 - p0, p2 - p0)));
//     // Ensure correct orientation of the geometric normal; follow the same
//     // approach as was used in Triangle::Intersect().
//     if (mesh->n) {
//         Normal3f ns(b[0] * mesh->n[v[0]] + b[1] * mesh->n[v[1]] +
//                     (1 - b[0] - b[1]) * mesh->n[v[2]]);
//         it.n = Faceforward(it.n, ns);
//     } else if (reverseOrientation ^ transformSwapsHandedness)
//         it.n *= -1;

//     // Compute error bounds for sampled point on triangle
//     Point3f pAbsSum =
//         Abs(b[0] * p0) + Abs(b[1] * p1) + Abs((1 - b[0] - b[1]) * p2);
//     it.pError = gamma(6) * Vector3f(pAbsSum.x, pAbsSum.y, pAbsSum.z);
//     *pdf = 1 / Area();
//     return it;
// }
      vec3f quadSamplePos = P + random()*du + random()*dv;
      float quadSamplePDF = 1.f/area;
      
      vec3f dir  = quadSamplePos - surface.P;
      float dist = length(dir);
      
      // Interaction Shape::Sample(const Interaction &ref, const Point2f &u,
      //                           Float *pdf) const {
      //     Interaction intr = Sample(u, pdf);
      //     Vector3f wi = intr.p - ref.p;
      //     if (wi.LengthSquared() == 0)
      //         *pdf = 0;
      //     else {
      //         wi = Normalize(wi);
      dir = normalize(dir);
      //         // Convert from area measure, as returned by the Sample() call
      //         // above, to solid angle measure.
      //         *pdf *= DistanceSquared(ref.p, intr.p) / AbsDot(intr.n, -wi);
      float shapePDF = quadSamplePDF * (dist*dist) / fabsf(dot(dir,N));

      //         if (std::isinf(*pdf)) *pdf = 0.f;
      //     }
      //     return intr;
      // }



      // Spectrum DiffuseAreaLight::Sample_Li(const Interaction &ref, const Point2f &u,
      //                                      Vector3f *wi, Float *pdf,
      //                                      VisibilityTester *vis) const {
      //   ProfilePhase _(Prof::LightSample);
      //   Interaction pShape = shape->Sample(ref, u, pdf);
      //   pShape.mediumInterface = mediumInterface;
      //   if (*pdf == 0 || (pShape.p - ref.p).LengthSquared() == 0) {
      //     *pdf = 0;
      //     return 0.f;
      //   }
      //   *wi = Normalize(pShape.p - ref.p);
      //   *vis = VisibilityTester(ref, pShape);
      //   return L(pShape, -*wi);
      // }

      light.dist = dist;
      light.dir  = dir;
      light.pdf  = shapePDF;
      light.L    = thisL;
      return true;
    }

    /* this will become a variant/any/union */
    typedef QuadLight AreaLight;
    
    inline AreaLight createAreaLight(const QuadLight &light) { return light; }
    
    struct AreaLightsList {
      QuadLight *lights     = 0;
      int        lightCount = 0;

      template<typename Random>
      inline __device__ bool sample(LightSample &light,
                                    const SurfaceSample &surface,
                                    Random &random) const;
    };

    template<typename Random>
    inline __device__ bool AreaLightsList::sample(LightSample &light,
                                                 const SurfaceSample &surface,
                                                 Random &random) const
    {
      if (lightCount == 0.f) return false;

      // for now, pick one randomly
      int lightID = min(int(lightCount*random()),lightCount-1);
      if (!lights[lightID].sample(light,surface,random))
        return false;
      light.pdf *= 1.f/lightCount;
      return true;
    }
    
  } // ::cup::pod
} // ::cup

