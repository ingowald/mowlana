// ======================================================================== //
// Copyright 2019-2020 Ingo Wald                                            //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#pragma once

#include "cup/pod/Materials.h"
#include "cup/pod/Textures.h"
#include <cuda_runtime_api.h>

namespace cup {
  namespace pod {

    struct MaterialData {
      cudaTextureObject_t *textureObjects = 0;
      Material            *materials = 0;
      Texture             *textures = 0;

      template<int MAX_DEPTH>
      inline __device__
      vec3f getDiffuseT(int materialID,
                        int primID,
                        const vec2f texcoord,
                        bool dbg=false) const;
      inline __device__
      vec3f getDiffuse(int materialID,
                       int primID,
                       vec2f texcoord,
                       bool dbg=false) const;

      inline __device__
      vec3f applyTexture(const vec3f &kd,
                         int textureID,
                         int primID,
                         vec2f texcoord,
                         bool dbg=false) const;
    };


#if 1
    //def __CUDACC__
    inline __device__
    vec2f getPTexQuadUVs(const int primID, const vec2f uv)
    {
      float r = uv.x;
      float g = uv.y;
      vec2f quadUV = primID & 1
        ? r * vec2f(1.f,1.f) + g * vec2f(0.f,1.f)
        : r * vec2f(1.f,0.f) + g * vec2f(1.f,1.f);
      return quadUV;
    }

    inline __device__
    vec3f MaterialData::applyTexture(const vec3f &kd,
                                     int textureID,
                                     int   primID,
                                     vec2f texcoord,
                                     bool _dbg)
       const
    {
      if (textureID < 0 || textures == nullptr) return kd;

      const Texture &texture = textures[textureID];
      if (dbg()) printf("texture type %i\n",texture.type);
      switch(texture.type) {
      case Texture::CONSTANT:
        return texture.constant.value;
      // case Texture::PTEXFILE: 
      case Texture::IMAGE: {
        if (!textureObjects) return kd;
        auto to = textureObjects[textureID];
        if (!to) return kd;
        return kd
#ifdef __CUDACC__
          * vec3f(tex2D<float4>(to,texcoord.x,texcoord.y))
#endif
          ;
      }
#ifdef __CUDACC__
      case Texture::PTEXFILE: {
        if (!textureObjects) return kd;
        auto to = textureObjects[textureID];
        if (!to) return kd;
        if (dbg()) printf("to %li\n",to);
        const int quadID = (primID/2);
        if (dbg()) printf("tc before quad %f %f\n",texcoord.x,texcoord.y);
        texcoord = getPTexQuadUVs(primID,texcoord);
        if (dbg()) printf("tc on quad %f %f\n",texcoord.x,texcoord.y);
        int res = texture.ptexFile.bakeRes;
        int numBlocksX = BAKING_ATLAS_WIDTH / res;
        int bx = quadID % numBlocksX;
        int by = quadID / numBlocksX;
        if (dbg()) printf("quad %i bx %i by %i res %i numpixy %i\n",quadID,bx,by,res,texture.ptexFile.numPixelsY);
        texcoord.x = (res*bx+(res-1)*texcoord.x+.5f)/float(BAKING_ATLAS_WIDTH);
        texcoord.y = (res*by+(res-1)*texcoord.y+.5f)/float(texture.ptexFile.numPixelsY);
        if (dbg()) printf("tc in atlas %f %f",texcoord.x,texcoord.y);
        // printf("quad %i -> %f %f\n",quadID,texcoord.x,texcoord.y);
        // texcoord.x += quadID%256;
        // texcoord.y += quadID/256;
        return kd * vec3f(tex2D<float4>(to,texcoord.x,texcoord.y));
      }
#endif
      default:
        return vec3f(1,1,1);
      }
      
      // if (!textureObjects) return kd;
      // auto to = textureObjects[textureID];
      // // if (dbg) printf("texture %i %lx\n",textureID,to);
      // if (!to) return kd;// * vec3f(0.12,0.079999998,0.079999998);;
    
      // return kd * vec3f(tex2D<float4>(to,texcoord.x,texcoord.y));
    }

    inline __device__ vec3f lerp(vec3f a, vec3f b, float t)
    {
      return (1.f-t)*a+t*b;
    }

    inline __device__ vec3f lerp(const vec3f a,
                                 const vec3f b,
                                 const vec3f v)
    { return (1.f-v)*a+v*b; }
  
    template<>
    inline __device__
    vec3f MaterialData::getDiffuseT<0>(int materialID,
                                       int primID,
                                       const vec2f texcoord,
                                       bool dbg)
      const
    {
      return vec3f(0,0,0);
    }
  
    template<int MAX_DEPTH>
    inline __device__
    vec3f MaterialData::getDiffuseT(int materialID,
                                    int primID,
                                    const vec2f texcoord,
                                    bool dbg)
       const
    {
      if (materialID < 0 || materials == nullptr)
        return vec3f(1.f); //,0,0);
      const cup::pod::Material &material = materials[materialID];
      switch (material.type) {
      case Material::MATTE: 
        return applyTexture(material.matte.kd,primID,material.matte.map_kd,texcoord);
      case Material::UBER: 
        return applyTexture(material.uber.kd,primID,material.uber.map_kd,texcoord);
      case Material::DISNEY: 
        return material.disney.color;
      case Material::MIRROR: 
        return vec3f(0.f);
      case Material::GLASS: 
        return vec3f(0.f);
      case Material::PLASTIC: 
        return applyTexture(material.plastic.kd,primID,material.plastic.map_kd,texcoord);
      case Material::TRANSLUCENT: 
        return applyTexture(material.translucent.kd,primID,material.translucent.map_kd,texcoord);
      case Material::MIX: 
        return
          lerp(getDiffuseT<MAX_DEPTH-1>(material.mix.material0,primID,texcoord),
               getDiffuseT<MAX_DEPTH-1>(material.mix.material1,primID,texcoord),
               material.mix.amount);
      default:
        return vec3f(0,1,0);
      };
    }


    
    inline __device__
    vec3f MaterialData::getDiffuse(int materialID,
                                   int primID,
                                   vec2f texcoord,
                                   bool dbg)
      const
    {
      return getDiffuseT<2>(materialID,primID,texcoord,dbg);
    }


    using owl::common::saturate;

    inline __device__ vec3f hue_to_rgb(float hue)
    {
      float s = saturate( hue ) * 6.0f;
      float r = saturate( fabsf(s - 3.f) - 1.0f );
      float g = saturate( 2.0f - fabsf(s - 2.0f) );
      float b = saturate( 2.0f - fabsf(s - 4.0f) );
      return vec3f(r, g, b); 
    }
    
    inline __device__ vec3f temperature_to_rgb(float t)
    {
      float K = 4.0f / 6.0f;
      float h = K - K * t;
      float v = .5f + 0.5f * t;    return v * hue_to_rgb(h);
    }
    
                                    
    inline __device__
    vec3f heatMap(float t)
    {
#if 1
      return temperature_to_rgb(t);
#else
      if (t < .25f) return lerp(vec3f(0.f,1.f,0.f),vec3f(0.f,1.f,1.f),(t-0.f)/.25f);
      if (t < .5f)  return lerp(vec3f(0.f,1.f,1.f),vec3f(0.f,0.f,1.f),(t-.25f)/.25f);
      if (t < .75f) return lerp(vec3f(0.f,0.f,1.f),vec3f(1.f,1.f,1.f),(t-.5f)/.25f);
      if (t < 1.f)  return lerp(vec3f(1.f,1.f,1.f),vec3f(1.f,0.f,0.f),(t-.75f)/.25f);
      return vec3f(1.f,0.f,0.f);
#endif
    }
  
#endif
    
  }
}
