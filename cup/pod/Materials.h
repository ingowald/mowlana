// ======================================================================== //
// Copyright 2019-2020 Ingo Wald                                            //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#pragma once

#include "cup/serialized/Scene.h"
#include <string.h>

namespace cup {
  namespace pod {

#ifdef __CUDA_ARCH__
    inline __device__ bool dbg() { return owl::getLaunchIndex()*2 == owl::getLaunchDims(); }
    inline __device__ bool crosshairs() {
      return
        owl::getLaunchIndex().x*2 == owl::getLaunchDims().x
        ||
        owl::getLaunchIndex().y*2 == owl::getLaunchDims().y
        ; }
#else
    inline bool dbg() { return false; }
    inline bool crosshairs() { return false; }
#endif
        
    /*! type for a texture ID; ID = -1 means 'no texture', else this
      indexes into the Scene::textures[] array */
    typedef int texture_id_t;
  
    struct MirrorMaterial {
      vec3f        kr { .9f };
      texture_id_t map_bump = -1;
    };

    struct GlassMaterial {
      vec3f kr { 1.f, 1.f, 1.f };
      vec3f kt { 1.f, 1.f, 1.f };
      float index { 1.5f };
    };
    
    struct TranslucentMaterial {
      vec3f transmit { 0.5f, 0.5f, 0.5f };
      vec3f reflect  { 0.5f, 0.5f, 0.5f };
      vec3f kd { 0.25, 0.25, 0.25 };
      texture_id_t map_kd;
    };

    struct PlasticMaterial {
      vec3f kd { .25f };
      texture_id_t map_kd = -1;
      vec3f ks { .25f };
      texture_id_t map_ks = -1;
      texture_id_t map_bump = -1;
      float roughness { 0.1f };
      texture_id_t map_rougness;
      bool remapRoughness { true };
    };

    struct UberMaterial {
      vec3f kd { .25f };
      texture_id_t map_kd = -1;
    
      vec3f ks { .25f };
      texture_id_t map_ks = -1;
    
      vec3f kr { 0.f };
      texture_id_t map_kr = -1;
      
      vec3f kt { 0.f };
      texture_id_t map_kt = -1;
      
      vec3f opacity { 1.f };
      texture_id_t map_opacity = -1;
      
      float alpha { 0.f };
      texture_id_t map_alpha = -1;

      float shadowAlpha { 0.f };
      texture_id_t map_shadowAlpha = -1;

      float index { 1.5f };
      float roughness { 0.1f };
      float uRoughness { 0.f };
      texture_id_t map_uRoughness = -1;
      float vRoughness { 0.f };
      texture_id_t map_vRoughness = -1;

      texture_id_t map_roughness = -1;
      texture_id_t map_bump = -1;
    };

    struct DisneyMaterial {
// MakeNamedMaterial "sandSimple"
    // "string type" ["disney"]
    // "rgb color" [0.59 0.59 0.61]
    // "float spectrans" [0.00]
    // "float clearcoatgloss" [1.00]
    // "float speculartint" [0.00]
    // "float eta" [1.17]
    // "float sheentint" [1.00]
    // "float metallic" [0.00]
    // "float anisotropic" [0.00]
    // "float clearcoat" [0.00]
    // "float roughness" [0.80]
    // "float sheen" [0.50]
    // "bool thin" ["true"]
    // "float difftrans" [0.00]
// "float flatness" [0.30]
      vec3f color          = { 0.59f, 0.59f, 0.61f };
      float specTrans      = 0.00f;
      float clearCoatGloss = 1.00f;
      float specularTint   = 0.00f;
      float eta            = 1.17f;
      float sheenTint      = 1.00f;
      float metallic       = 0.00f;
      float anisotropic    = 0.00f;
      float clearCoat      = 0.00f;
      float roughness      = 0.80f;
      float sheen          = 0.50f;
      bool thin            = true;
      float diffTrans      = 0.00f;
      float flatness       = 0.30f;
    };
  
    struct MatteMaterial {
      vec3f        kd;
      texture_id_t map_kd = -1;
      float        sigma;
      texture_id_t map_sigma = -1;
      texture_id_t map_bump = -1;
    };
  
    struct MetalMaterial {
      float        roughness;
      vec3f        eta;
      vec3f        k;
    };
  
    struct MixMaterial {
      int material0 = -1;
      int material1 = -1;
      vec3f amount;
      texture_id_t map_amount;
    };
  
    struct Material {
      typedef enum { UNKNOWN,
                     MATTE,
                     METAL,
                     TRANSLUCENT,
                     GLASS,
                     DISNEY,
                     MIX,
                     MIRROR,
                     PLASTIC,
                     UBER
      } Type;
      
      Material() {}
      Material(const Material &other) = default;
    
      Material &operator=(const Material &other)
      { memcpy(this,&other,sizeof(other)); return *this; }
    
      Material(const DisneyMaterial &_disney)
        : type(DISNEY), disney(_disney)
      {}
      Material(const MatteMaterial &_matte)
        : type(MATTE), matte(_matte)
      {}
      Material(const MetalMaterial &_metal)
        : type(METAL), metal(_metal)
      {}
      Material(const GlassMaterial &_glass)
        : type(GLASS), glass(_glass)
      {}
      Material(const TranslucentMaterial &_translucent)
        : type(TRANSLUCENT), translucent(_translucent)
      {}
      Material(const MixMaterial &_mix)
        : type(MIX), mix(_mix)
      {}
      Material(const MirrorMaterial &_mirror)
        : type(MIRROR), mirror(_mirror)
      {}
      Material(const PlasticMaterial &_plastic)
        : type(PLASTIC), plastic(_plastic)
      {}
      Material(const UberMaterial &_uber)
        : type(UBER), uber(_uber)
      {}
    
      int type = UNKNOWN;
      union {
        MatteMaterial matte;
        MetalMaterial metal;
        GlassMaterial glass;
        TranslucentMaterial translucent;
        DisneyMaterial disney;
        MixMaterial mix;
        MirrorMaterial mirror;
        PlasticMaterial plastic;
        UberMaterial uber;
      };
    };

    MatteMaterial   createMaterial(const serialized::Scene *scene,
                                   pbrt::MatteMaterial::SP pbrt);
    MetalMaterial   createMaterial(const serialized::Scene *scene,
                                   pbrt::MetalMaterial::SP pbrt);
    MixMaterial     createMaterial(const serialized::Scene *scene,
                                   pbrt::MixMaterial::SP pbrt);
    MirrorMaterial  createMaterial(const serialized::Scene *scene,
                                   pbrt::MirrorMaterial::SP pbrt);
    PlasticMaterial createMaterial(const serialized::Scene *scene,
                                   pbrt::PlasticMaterial::SP pbrt);
    UberMaterial    createMaterial(const serialized::Scene *scene,
                                   pbrt::UberMaterial::SP pbrt);
    Material        createMaterial(const serialized::Scene *scene,
                                   pbrt::Material::SP pbrtMat);
  
  } // ::cup::pod
} // ::cup
