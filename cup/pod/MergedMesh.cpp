// ======================================================================== //
// Copyright 2019-2020 Ingo Wald                                            //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#include "cup/pod/MergedMesh.h"

namespace cup {
  namespace pod {
    
    void MergedMeshBuilder::build(/*! the scene we can look up linear material
                                    IDs, texture IDs, etc, in */
                                  const serialized::Scene *scene,
                                  /*! the shapes we're going to build this merged
                                    mesh over */
                                  const std::vector<pbrt::Shape::SP> &shapes)
    {
      index.clear();
      vertex.clear();
      normal.clear();
      texcoord.clear();
      subMesh.clear();

      size_t numVertices = 0;
      size_t numIndices = 0;
      bool hasNormals = 0;
      bool hasTexcoords = 0;
      // -------------------------------------------------------
      // first pass - discover and count
      // -------------------------------------------------------
      std::vector<pbrt::TriangleMesh::SP> meshes;
      std::vector<int> vertexOffsets;
      std::vector<int> indexOffsets;
      for (auto shape : shapes) {
        if (!shape) continue;
        pbrt::TriangleMesh::SP mesh = shape->as<pbrt::TriangleMesh>();
        if (!mesh) continue;

        pod::SubMesh mm;
        // mm.globalMeshID     = scene->getIdx(shape);
        // mm.globalMaterialID = scene->getIdx(shape->material);
        mm.materialID       = scene->getIdx(shape->material);
        mm.alphaTextureID   = -1;
        // mm.bumpTextureID    = -1;
        mm.colorTextureID   = -1;
        mm.alphaTexture     = 0;
        
        mm.hasNormals       = !mesh->normal.empty();
        mm.hasTexcoords     = !mesh->texcoord.empty();

        mm.indexOffset      = numIndices;
        mm.indexCount       = mesh->index.size();
        mm.shapeID          = scene->getIdx(mesh);
           

        auto alphaTex = shape->textures.find("alpha");
        if (alphaTex != shape->textures.end()) 
          mm.alphaTextureID    = scene->getIdx(alphaTex->second);

        auto colorTex = shape->textures.find("color");
        if (colorTex != shape->textures.end()) {
          mm.colorTextureID    = scene->getIdx(colorTex->second);
          // if (mm.colorTextureID >= 0)
          //   mm.colorTextureIsBaked
          //     = (scene->textures[mm.colorTextureID]->as<pbrt::PtexFileTexture>() != 0);
        }
        
        vertexOffsets.push_back((int)numVertices);
        indexOffsets.push_back((int)numIndices);
        meshes.push_back(mesh);
        subMesh.push_back(mm);

        hasNormals   |= mm.hasNormals;
        hasTexcoords |= mm.hasTexcoords;
        numIndices  += mesh->index.size();
        numVertices += mesh->vertex.size();
      }

      // -------------------------------------------------------
      // second pass - execute
      // -------------------------------------------------------
      index.resize(numIndices);
      vertex.resize(numVertices);
      if (hasNormals)
        normal.resize(numVertices);
      if (hasTexcoords)
        texcoord.resize(numVertices);
      
      owl::parallel_for
        (meshes.size(),
         [&](size_t subMeshID){
          const pod::SubMesh &sm     = subMesh[subMeshID];
           auto  mesh                 = meshes[subMeshID];
           const int thisVertexOffset = vertexOffsets[subMeshID];
           const int thisIndexOffset  = indexOffsets[subMeshID];

           // ------------------------------------------------------------------
           // write new indices
           // ------------------------------------------------------------------
           for (size_t i=0;i<mesh->index.size();i++)
             this->index[thisIndexOffset+i] 
               = { ((const vec3i&)mesh->index[i]+thisVertexOffset), (int)subMeshID };
           
           // ------------------------------------------------------------------
           // and copy vertices, as applicable (note can't use
           // std::copy due to different types)
           // ------------------------------------------------------------------
           for (int i=0;i<mesh->vertex.size();i++)
             this->vertex[i+thisVertexOffset] = (const vec3f&)mesh->vertex[i];
           for (int i=0;i<mesh->normal.size();i++)
             this->normal[i+thisVertexOffset] = (const vec3f&)mesh->normal[i];
           for (int i=0;i<mesh->texcoord.size();i++)
             this->texcoord[i+thisVertexOffset] = (const vec2f&)mesh->texcoord[i];

           // mesh->vertex.clear();
           // mesh->normal.clear();
           // mesh->texcoord.clear();
         });
    }
    
  } // ::cup::pod
} // ::cup
