// ======================================================================== //
// Copyright 2019-2020 Ingo Wald                                            //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#pragma once

#include "cup/serialized/Scene.h"
#include "cup/pod/ImageTextures.h"
#include <string.h>

namespace cup {
  namespace pod {
    
    /*! type for a texture ID; ID = -1 means 'no texture', else this
      indexes into the Scene::textures[] array */
    typedef int texture_id_t;
  
    struct ImageTexture {
      int cudaTextureID=-1;
    };
    
    struct PtexFileTexture {
      int cudaTextureID=-1;
      int bakeRes;
      int numPixelsY;
    };
    
    struct ConstantTexture {
      vec3f value;
    };

    struct Texture {
      typedef enum { UNKNOWN, CONSTANT, PTEXFILE, IMAGE } Type;
      int type = UNKNOWN;

      Texture() : type(UNKNOWN) {}
      Texture(const ConstantTexture &tex) : type(CONSTANT), constant(tex) {}
      Texture(const ImageTexture &tex) : type(IMAGE), image(tex) {}
      Texture(const PtexFileTexture &tex) : type(PTEXFILE), ptexFile(tex) {}
      
      Texture &operator=(const Texture &other)
      { memcpy(this,&other,sizeof(other)); return *this; }
    
      union {
        ImageTexture    image;
        PtexFileTexture ptexFile;
        ConstantTexture constant;
      };
    };

    Texture createTexture(const serialized::Scene *scene,
                          pbrt::Texture::SP pbrtMat,
                          const std::vector<ImageTextureHost> &hostTextures);

  } // ::cup::pod
} // ::cup
