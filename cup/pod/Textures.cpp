// ======================================================================== //
// Copyright 2019-2020 Ingo Wald                                            //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#include "cup/pod/Textures.h"

namespace cup {
  namespace pod {
  
    ImageTexture createTexture(const serialized::Scene *scene,
                               pbrt::ImageTexture::SP pbrt)
    {
      ImageTexture image;
      image.cudaTextureID = scene->getIdx(pbrt);
      return image;
    }

    PtexFileTexture createTexture(const serialized::Scene *scene,
                                  pbrt::PtexFileTexture::SP pbrt,
                                  const std::vector<ImageTextureHost> &hostTextures)
    {
      PtexFileTexture ptexFile;
      ptexFile.cudaTextureID = scene->getIdx(pbrt);
      // PING;
      // PRINT(ptexFile.cudaTextureID);
      // PRINT(hostTextures.size());
      if (ptexFile.cudaTextureID >= 00 &&
          ptexFile.cudaTextureID < hostTextures.size()) {
        ptexFile.bakeRes = hostTextures[ptexFile.cudaTextureID].bakeRes;
        ptexFile.numPixelsY = hostTextures[ptexFile.cudaTextureID].size.y;
      }
      return ptexFile;
    }

    ConstantTexture createTexture(const serialized::Scene *scene,
                                  pbrt::ConstantTexture::SP pbrt)
    {
      ConstantTexture constant;
      constant.value = (const vec3f&)pbrt->value;
      return constant;
    }


    Texture createTexture(const serialized::Scene *scene,
                          pbrt::Texture::SP pbrtTex,
                          const std::vector<ImageTextureHost> &hostTextures)
    {
      std::string thisType = pbrtTex->toString();
      static std::set<std::string> alreadyWarned;

      if (auto tex = pbrtTex->as<pbrt::ConstantTexture>())
        return createTexture(scene,tex);
      if (auto tex = pbrtTex->as<pbrt::ImageTexture>())
        return createTexture(scene,tex);
      if (auto tex = pbrtTex->as<pbrt::PtexFileTexture>())
        return createTexture(scene,tex,hostTextures);
    
      if (alreadyWarned.find(thisType) == alreadyWarned.end()) {
        alreadyWarned.insert(thisType);
        std::cout << OWL_TERMINAL_LIGHT_RED
                  << "#pod: Warning: un-recognized PBRT texture type '"
                  << thisType
                  << "' (more instances may follow)"
                  << OWL_TERMINAL_DEFAULT
                  << std::endl;
      }
      return Texture();
    }
  
  }  
}

