// ======================================================================== //
// Copyright 2019-2020 Ingo Wald                                            //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#pragma once

#include "cup/serialized/Scene.h"

#define BAKING_ATLAS_WIDTH 2048

namespace cup {
  namespace pod {

    /*! a POD image texture; this one only stores the pointer, but
        doesn't say how it was allocated, or how it is to be freed */
    struct ImageTextureHost {
      ~ImageTextureHost();

      bool  isBakedPtex = false;
      vec2i size;
      union {
        int      numChannels;
        struct {
          int      bakeRes;
          int      numPrims; // only for baking
        };
      };
      unsigned char *texels = nullptr;
    };

    /*! tries to load all textures in the serialized scene as image
        textures; the result vector will alwys be the same size as
        scene->textures, but those textures that are either not image
        textures or that cannot be loaded will be set to a
        pos::ImageTexture with 0 size and null texels pointer */ 
    void loadImageTextures(std::vector<ImageTextureHost> &hostTextures,
                           const cup::serialized::Scene::SP &scene,
                           const std::string &texturePath);

    /*! given the currently active GPU, create a list of cuda texture
        objects for the given vector of host textures */
    void createTextureObjects(std::vector<cudaArray_t> &cudaArrays,
                              std::vector<cudaTextureObject_t> &cudaTextures,
                              const std::vector<ImageTextureHost> &hostTextures);
    
  } // ::cup::pod
} // ::cup
