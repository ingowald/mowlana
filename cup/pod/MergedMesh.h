// ======================================================================== //
// Copyright 2019-2020 Ingo Wald                                            //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#pragma once

#include "cup/cup.h"
#include "cup/serialized/Scene.h"

namespace cup {
  namespace pod {

    struct OWL_ALIGN(16) SubMesh {
      union {
        struct {
          cudaTextureObject_t alphaTexture;
          // struct {
            // uint32_t hasNormals   : 1;
            // uint32_t hasTexcoords : 1;
          bool hasNormals;
          bool hasTexcoords;
          // };
          short                 alphaTextureID;
          short                 colorTextureID;
          // int                 bumpTextureID ;
          int                 materialID    ;
          int                 shapeID       ;
          int                 indexOffset;
          int                 indexCount;
        };
// #ifdef __CUDACC__
        struct {
          uint4 aligned0;
          uint4 aligned1;
        };
// #endif
      };
    };
    
    /*! this has the cuda texture directly baked into the submesh, and
        otherwise only stores material ID and something that the user
        can set to whever he/she likes (probably a global mesh ID,
        shape ID, etc */
    struct MergedMesh {
      inline __device__ SubMesh getSubMesh(int meshID) const {
        return subMesh[meshID];
      }
      
      const vec3f   *vertex    = 0;
      const vec3f   *normal    = 0;
      const float2  *texcoord  = 0;
      //! fourth component is submesh ID
      const int4    *index     = 0;
      const SubMesh *subMesh;
    };
    
    struct MergedMeshBuilder {
      struct Index {
        /*! vertex IDs into the *merged* vertex array(s); note for
          normals and texcoords that some meshes may not actually have
          valid data in those arrays */
        vec3i vertexIdx;
        /*! the index into the submesh array */
        int   subMeshIdx;
      };

      /*! build this merged mesh over the given shapes, using the
          provided servialized scnee to compute texture and mateiral
          indices.  */
      void build(/*! the scene we can look up linear material
                   IDs, texture IDs, etc, in */
                 const serialized::Scene *scene,
                 /*! the shapes we're going to build this merged
                   mesh over */
                 const std::vector<pbrt::Shape::SP> &shapes);
    
      std::vector<Index>   index;
      std::vector<vec3f>   vertex;
      std::vector<vec3f>   normal;
      std::vector<vec2f>   texcoord;
      std::vector<SubMesh> subMesh;
    };
  
  } // ::cup::pod
} // ::cup
