// ======================================================================== //
// Copyright 2019-2020 Ingo Wald                                            //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#pragma once

#include "cup/pod/Materials.h"
#include <cuda_runtime_api.h>

namespace cup {
  namespace sampling {
    
    template<typename RandomNumberGenerator>
    inline __device__
    vec3f randomDirection(RandomNumberGenerator &random)
    {
      while (1) {
        vec3f d(random(),random(),random());
        if (dot(d,d) <= 1.f) return normalize(d);
      };
    }
  
  }
}
