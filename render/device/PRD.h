// ======================================================================== //
// Copyright 2019-2020 Ingo Wald                                            //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#pragma once

#include "render/device/common.h"
#include "owl/owl.h"
#include "owl/common/math/random.h"
#include "cup/pod/Materials.h"
#include "cup/pod/MergedMesh.h"
#include <cuda.h>

namespace dev {
  
  struct OWL_ALIGN(16) PRD {
    float hit_t;
    vec3f Ng, Ns;
    vec3f P;
    vec2f uv;
    int   primID;
    int   instID;
    const cup::pod::SubMesh *subMesh;
#if MEASURE_ANYHIT_CALLS
    // 'linear' sum in the sense that every thread adds '1'
    int   numAnyHitCalls;
    // every thread add sub of active threads
    int   sumWarpActiveAnyHit;
    int   numWarpActiveAnyHit;
    
#endif
    int   materialID;
    int   colorTextureID;
    int   firstPrimID;
  };
  
}
