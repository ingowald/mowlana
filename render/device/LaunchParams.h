// ======================================================================== //
// Copyright 2019-2020 Ingo Wald                                            //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#pragma once

#include "render/device/PRD.h"
#include "cup/pod/MaterialData.h"
#include "owl/owl_device_buffer.h"

namespace dev {
  
#if __CUDA_ARCH__
  typedef owl::RayT<0,2> Ray;
  typedef owl::RayT<1,2> ShadowRay;
#endif

  using namespace cup::pod;
  typedef owl::common::LCG<8> Random;

  // debugging for nate
  struct MyFunnyType {
    int foo[123];
  };
  
  struct Camera {
#if __CUDA_ARCH__
    inline __device__ Ray generateRay(const vec2f &screen);
#endif
    vec3f origin;
    vec3f screen_00;
    vec3f screen_du;
    vec3f screen_dv;
  };
  
  struct LPs {
    int deviceID;
    int numDevices;
    int       accumID;
    float4   *accumBuffer;
    uint32_t *colorBuffer;
    cup::pod::MaterialData materialData;
    vec2i     fbSize;

    int shadeMode = 0;
    
    Camera camera;
    int samplesPerPixel;
    int maxNumBounces;
    float heatMapScale;
    vec2i debugPixel;

    cudaTextureObject_t    envLightTexture;
    affine3f               envLightTransform;
    vec3f                  envLightPower;
    
    OptixTraversableHandle world;
  };

#ifdef __CUDA_ARCH__
  extern "C" __constant__ OWL_ALIGN(16) LPs optixLaunchParams;
#endif
}
