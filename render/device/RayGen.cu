// ======================================================================== //
// Copyright 2019-2020 Ingo Wald                                            //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#include "render/device/LaunchParams.h"
#include "cup/sampling/directions.h"

namespace dev {

  using namespace cup::pod;
  using namespace cup::sampling;
  
  /*! stolen from pete shirley's ray tracing in a weekend */
  inline __device__
  float schlick(float cosine, float ref_idx)
  {
    float r0 = (1.f-ref_idx) / (1.f+ref_idx);
    r0 = r0*r0;
    return r0 + (1.f-r0)*powf((1.f - cosine),5.f);
  }

  /*! stolen from pete shirley's ray tracing in a weekend */
  inline __device__
  bool refract(const vec3f &v, const vec3f &n, float ni_over_nt, vec3f& refracted)
  {
    vec3f uv = v; //already normalized in our code unit_vector(v);
    float dt = dot(uv, n);
    float discriminant = 1.0 - ni_over_nt*ni_over_nt*(1-dt*dt);
    if (discriminant > 0) {
      refracted = ni_over_nt*(uv - n*dt) - n*sqrtf(discriminant);
      return true;
    }
    else 
      return false;
  }

  /*! stolen from pete shirley's ray tracing in a weekend */
  inline __device__
  vec3f reflect(const vec3f& v, const vec3f& n)
  {
    return v - 2*dot(v,n)*n;
  }
    
    
  struct DirSample {
    vec3f w_o;
    float pdf;
  };

  struct BRDFSample : public DirSample {
    vec3f brdf;
  };

  /*! differential geometry */
  struct DG {
    /*! shading normal, world space, normalized, NOT faceforwarded */
    vec3f Ns;
    /*! geometry normal, world space, normalized, NOT faceforwarded */
    vec3f Ng;

    vec3f Nsff, Ngff;
    
    float currentMedium;
    vec3f diffuseTexture;
  };

  typedef enum {
                SAMPLED_SPECULAR,
                SAMPLED_DIFFUSE,
                SAMPLED_ERROR
  } SampleType;


  inline __device__
  void faceForward(vec3f &Ngff,
                   const vec3f &Ng,
                   vec3f &Nsff,
                   const vec3f &Ns,
                   const vec3f w_i)
  {
    Ngff = Ng;
    Nsff = Ns;
    
    // first, move shading normal on same side of geometry normal
    if (dot(Ngff,Nsff) < 0.f) Nsff = -Nsff;
    
    // now, flip both to side of ray
    if (dot(Ngff,w_i) > 0.f) {
      Ngff = -Ngff;
      Nsff = -Nsff;
    }
  }
  
  /*! stolen from pete - should replace with better sampler from GI
    compendium */
  inline __device__
  vec3f sampleCosineWeightedHemisphere(Random &random)
  {
    while (1) {
      vec3f p = 2.f*vec3f(random(),random(),random()) - vec3f(1.f);
      if (dot(p,p) > 1.f) continue;
      return normalize(normalize(p)+vec3f(0.f,0.f,1.f));
    }
  }
  
  
  inline __device__
  void sampleCosineWeightedHemisphere(DirSample &out,
                                      const vec3f &Ngff,
                                      const vec3f &Nsff,
                                      Random &random)
  {
    linear3f frame = owl::common::frame(Ngff);
    vec3f dir = sampleCosineWeightedHemisphere(random);
    out.w_o = normalize(dir.x*frame.vx +
                        dir.y*frame.vy +
                        dir.z*Nsff);
    out.pdf = 1.f;
  }
  
  inline __device__
  SampleType sample(const DisneyMaterial &material,
                    BRDFSample &out,
                    /*! ior of current medium */
                    DG &dg,
                    /*! direction of INCOMING ray (ie,
                      facing TOWARDS the surface) */
                    Random &random,
                    const vec3f w_i)
  {
    const float specularProb = material.specTrans;
    // = (material.specTrans.x+material.specTrans.y+material.specTrans.z)
    // / 3.f;
    float r0 = random();
    if (r0 >= specularProb) {
      /* ******************************************************* */
      /* DIFFUSE interaction                                     */
      /* ******************************************************* */
      sampleCosineWeightedHemisphere(out,dg.Ngff,dg.Nsff,random);
      vec3f matColor = material.color;
      if (matColor == vec3f(1.f,0.f,0.f) ||
          matColor == vec3f(0.f,1.f,1.f) ||
          matColor == vec3f(1.f,0.f,1.f) ||
          matColor == vec3f(1.f,1.f,0.f)
          )
        matColor = vec3f(1.f);
      out.brdf = matColor*dg.diffuseTexture;
      out.pdf *= 1.f-specularProb;
      return SAMPLED_DIFFUSE;
    } else {
      /* ******************************************************* */
      /* SPECULAR interaction                                    */
      /* ******************************************************* */
      const bool entering = dot(w_i,dg.Ng) < 0.f;
      const bool leaving  = !entering;

      if (dbg())
        printf("entering? %i leaving? %i currentMedium %f materailMedium %f\n",
               int(entering),int(leaving),
               dg.currentMedium,material.eta);
      /* check if we're hitting same medium boundary twice - moana
         has soem of the water surface twice, so that's a
         possiblity. if so, just shoot through */
      if (
          /* re-entering same medium? */
          (entering && dg.currentMedium != 1.f && dg.currentMedium == material.eta) ||
          /* re-leaving same medium? */
          (leaving  && dg.currentMedium == 1.f)) {
        out.pdf  = specularProb; //1.f;
        out.brdf = 1.f;
        out.w_o  = w_i;
        if (dbg()) printf("->PASSING RIGHT THROUGH\n");
        return SAMPLED_SPECULAR;
      } else {
        /* this here stolen largely from pete shirley's ray tracing in a
          weekend, the dielectric material */
        // virtual bool scatter(const ray& r_in,
        //                      const hit_record& rec,
        //                      vec3& attenuation,
        //                      ray& scattered) const  {
        vec3f attenuation;
        vec3f outward_normal;
        vec3f reflected = reflect(w_i,dg.Ns);
        float ni_over_nt;
        attenuation = vec3f(1.f, 1.f, 1.f); 
        vec3f refracted;
        float reflect_prob;
        float cosine;
        vec3f scattered;
        const float ref_idx = material.eta;
        if (dot(w_i, dg.Ns) > 0.f) {
          outward_normal = -dg.Ns;
          ni_over_nt = ref_idx;
          cosine = dot(w_i, dg.Ns);
          cosine = sqrtf(1.f - ref_idx*ref_idx*(1.f-cosine*cosine));
        }
        else {
          outward_normal = dg.Ns;
          ni_over_nt = 1.f / ref_idx;
          cosine = -dot(w_i, dg.Ns);
        }
        if (refract(w_i, outward_normal, ni_over_nt, refracted)) {
          reflect_prob = schlick(cosine, ref_idx);
        } else {
          reflect_prob = 1.f;
        }
        if (random() <= reflect_prob) {
          scattered = reflected;
          if (dbg()) printf("->reflected\n");
        } else {
          scattered = refracted;
          if (entering) dg.currentMedium = material.eta;
          else          dg.currentMedium = 1.f;
          if (dbg()) printf("->refracted, new medium %f\n",dg.currentMedium);
        }
        out.w_o  = scattered;
        out.brdf = material.specTrans * attenuation;
        out.pdf  = specularProb;
        
        if (dbg()) printf(" => specular %f %f %f / %f\n",
                          out.brdf.x,
                          out.brdf.y,
                          out.brdf.z,
                          out.pdf
                          );
        
        return SAMPLED_SPECULAR;
      }
    }
  }
                                  


  inline __device__ Ray Camera::generateRay(const vec2f &screen)
  {
    const vec3f dir
      = screen_00
      + screen.x * screen_du
      + screen.y * screen_dv;
    return Ray(origin,normalize(dir),1e-3f,1e10f);
  }

  inline __device__ float clampf(float f, float lo, float hi)
  { return max(lo,min(hi,f)); }
  
  inline __device__ float pbrtSphericalTheta(const vec3f &v) {
    return acosf(clampf(v.z, -1.f, 1.f));
  }
  
  inline __device__ float pbrtSphericalPhi(const vec3f &v) {
    float p = atan2f(v.y, v.x);
    return (p < 0.f) ? (p + float(2.f * M_PI)) : p;
  }

  inline __device__ vec3f irradianceFromEnvironment(vec3f dir)
  {
    auto &lp = optixLaunchParams;
    if (lp.envLightTexture) {
      vec3f d = xfmVector(lp.envLightTransform,-dir);
      float theta = pbrtSphericalTheta(d);
      float phi   = pbrtSphericalPhi(d);
      const float invPi = 1.f/M_PI;
      const float inv2Pi = 1.f/(2.f*M_PI);
      vec2f uv(phi * inv2Pi, theta * invPi);

      // if (dbg())
      //   printf("dir %f %f %f -> u %f v %f\n",
      //          dir.x,dir.y,dir.z,uv.x,uv.y);
      vec4f color = tex2D<float4>(lp.envLightTexture,uv.x,uv.y);
      return vec3f(color)*lp.envLightPower;
    } else return vec3f(1.f);
  }
  
  inline __device__
  vec3f backgroundColor(vec3f dir)
  {
    auto &lp = optixLaunchParams;
    if (lp.envLightTexture) {
      return irradianceFromEnvironment(dir);
    } else {
      const vec2i pixelID = owl::getLaunchIndex();
      const float t = pixelID.y / (float)optixGetLaunchDimensions().y;
      const vec3f c = (1.0f - t)*vec3f(1.0f, 1.0f, 1.0f) + t * vec3f(0.5f, 0.7f, 1.0f);
      return c;
    }
  }

  
  OPTIX_RAYGEN_PROGRAM(renderFrame)()
  {
    const vec2i pixelID = owl::getLaunchIndex();
    const int pixelIdx = pixelID.x + owl::getLaunchDims().x * pixelID.y;

    auto &lp = optixLaunchParams;

    // for multi-device:
    if (((pixelID.x >> 5) % lp.numDevices) != lp.deviceID)
      return;
    if (pixelID.x >= optixGetLaunchDimensions().x) return;
    if (pixelID.y >= optixGetLaunchDimensions().y) return;

    uint64_t clock_begin = clock();

    Random random;
    random.init(pixelIdx,lp.accumID);
    PRD prd;
    
    vec3f accumColor = 0.f;

    int numDiffuseBounces = 0;
    if (dbg()) printf("=======================================================\n");
    for (int sampleID=0;sampleID<lp.samplesPerPixel;sampleID++) {
      vec2f samplePos
        = (lp.accumID == 0 && sampleID == 0)
        ? vec2f(.5f)
        : vec2f(random(),random());
      const vec2f screen = (vec2f(pixelID)+samplePos);
      Ray ray = lp.camera.generateRay(screen);
      vec3f pathWeight = 1.f;

      DG dg;
      dg.currentMedium = 1.f;
      for (int pathDepth=0;true;pathDepth++) {
        if (dbg()) printf("----------- depth %i weight %f %f %f -----------\n",pathDepth,
                          pathWeight.x,pathWeight.y,pathWeight.z);
        if (pathDepth > 20) break;
        
        prd.primID = -1;
        prd.hit_t = ray.tmax;
        if (dbg()) printf("ray %f %f %f dir %f %f %f\n",
                          ray.origin.x,
                          ray.origin.y,
                          ray.origin.z,
                          ray.direction.x,
                          ray.direction.y,
                          ray.direction.z
                          );
        owl::traceRay(lp.world,ray,prd);

        if (dbg()) printf("hit %i %f\n",prd.primID,prd.hit_t);
        if (prd.primID < 0) {
          if (pathDepth == 0) 
            accumColor += backgroundColor(ray.direction);
          else {
            const vec3f contrib = pathWeight * irradianceFromEnvironment(ray.direction);
            if (dbg())
              printf("adding %f %f %f\n",contrib.x,contrib.y,contrib.z);
            accumColor += contrib;
          }
          break;
        }
        // if (dot(prd.Ns,ray.direction) > 0.f)
        //   prd.Ns = -prd.Ns;
        // prd.P += 1e-3f*prd.Ns;

        // if (maxNumBounces > 0 && numBounces >= maxNumBounces)
        //   break;

        dg.Ng = prd.Ng;
        dg.Ns = prd.Ns;
        faceForward(dg.Ngff,dg.Ng,dg.Nsff,dg.Ns,ray.direction);

        const cup::pod::Material &hitMaterial
          = lp.materialData.materials[prd.materialID];

        DisneyMaterial material = hitMaterial.disney;
        if (hitMaterial.type != Material::DISNEY) {
          material.eta = 1.f;
          material.specTrans = 0.f;
          material.color = .6f;
        }

        dg.diffuseTexture
          = lp.materialData.applyTexture(vec3f(1.f),
                                         prd.subMesh->colorTextureID,
                                         prd.primID,
                                         prd.uv);
        if (dg.diffuseTexture != vec3f(1.f))
          material.color = vec3f(1.f);
        
        if (dbg())
          printf("material ID %i type %i spectrans %f eta %f\n color %f %f %f\n tex %f %f %f\n",
                 prd.materialID,
                 hitMaterial.type,
                 material.specTrans,
                 material.eta,
                 material.color.x,
                 material.color.y,
                 material.color.z,
                 dg.diffuseTexture.x,
                 dg.diffuseTexture.y,
                 dg.diffuseTexture.z);
        
        BRDFSample brdfSample;
        SampleType sampleType
          = sample(material,
                   brdfSample,
                   dg,random,ray.direction);

        if (dbg())
          printf("sampled type %i dir %f %f %f brdf %f %f %f pdf %f\n",
                 (int)sampleType,
                 brdfSample.w_o.x,
                 brdfSample.w_o.y,
                 brdfSample.w_o.z,
                 brdfSample.brdf.x,
                 brdfSample.brdf.y,
                 brdfSample.brdf.z,
                 brdfSample.pdf);
        if (sampleType == SAMPLED_DIFFUSE) {
          if (++numDiffuseBounces > 2)
            break;
        }
        
        pathWeight *= (brdfSample.brdf / brdfSample.pdf);
        ray.origin = prd.P;// + 1e-3f*dg.Ngff;
        ray.direction = normalize(brdfSample.w_o);
        if (dot(brdfSample.w_o,dg.Ng) > 0.f)
          ray.origin += 1e-3f*dg.Ng;
        else
          ray.origin -= 1e-3f*dg.Ng;
        
        ray.tmin = 1e-3f;
        ray.tmax = 1e20f;
      }
    }
    accumColor *= (1.f/lp.samplesPerPixel);

    uint64_t clock_end = clock();
    if (lp.heatMapScale > 0.f) {
      float t = (clock_end-clock_begin)*lp.heatMapScale;
      accumColor = heatMap(t);
    }
    
    if (crosshairs()) accumColor = 1.f - accumColor;
    
    float4 *accumBuffer = (float4*)lp.accumBuffer;
    if (lp.accumID > 0)
      accumColor += (vec3f)accumBuffer[pixelIdx];
    accumBuffer[pixelIdx] = vec4f(accumColor,0.f);
    accumColor *= 1.f/(lp.accumID+1.f);
    // "tone map"
    const uint32_t rgba = owl::make_rgba(sqrt(accumColor));
    lp.colorBuffer[pixelIdx] = rgba;
  }
  
}

