// ======================================================================== //
// Copyright 2019-2020 Ingo Wald                                            //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#include "render/device/TriMesh.h"
#include "render/device/LaunchParams.h"
#include <cooperative_groups.h>

using namespace cooperative_groups;

namespace dev {

  inline __device__ vec3f randomDirection(Random &random)
  {
    while (1) {
      vec3f d(random(),random(),random());
      if (dot(d,d) <= 1.f) return normalize(d);
    };
  }

  OPTIX_ANY_HIT_PROGRAM(TriMesh)()
  {
    auto &self = owl::getProgramData<cup::pod::MergedMesh>();
    
    const int   primID = optixGetPrimitiveIndex();
    const int4 index = self.index[primID];
    const cup::pod::SubMesh sm = self.getSubMesh(index.w);

    if (sm.alphaTexture) {

      vec2f tc = optixGetTriangleBarycentrics();
      if (sm.hasTexcoords)
        tc
          = (1.f-tc.u-tc.v) * vec2f(self.texcoord[index.x])
          +      tc.u       * vec2f(self.texcoord[index.y])
          +           tc.v  * vec2f(self.texcoord[index.z]);
      float alpha = tex2D<float4>(sm.alphaTexture,tc.u,tc.v).w;

      if (alpha < 1e-3f)
        optixIgnoreIntersection();
    }
  }

  OPTIX_CLOSEST_HIT_PROGRAM(TriMesh)()
  {
    // auto &lp   = optixLaunchParams;
    auto &prd  = owl::getPRD<PRD>();
    auto &self = owl::getProgramData<cup::pod::MergedMesh>();
    const vec3f rayOrg = optixGetWorldRayOrigin();
    const vec3f rayDir = optixGetWorldRayDirection();
    const float rayT   = optixGetRayTmax();
    const float u = optixGetTriangleBarycentrics().x;
    const float v = optixGetTriangleBarycentrics().y;
    
    const int   primID = optixGetPrimitiveIndex();
    const int4  index  = self.index[primID];
    const cup::pod::SubMesh sm = self.getSubMesh(index.w);
    
    prd.hit_t      = rayT;
    prd.materialID = sm.materialID;
    prd.firstPrimID = sm.indexOffset;
    prd.primID     = primID - prd.firstPrimID;
    prd.subMesh    = &sm;
    
    const vec3f A = self.vertex[index.x];
    const vec3f B = self.vertex[index.y];
    const vec3f C = self.vertex[index.z];
    
    vec3f Ng = cross(B-A,C-A);
    Ng = normalize((vec3f)optixTransformNormalFromObjectToWorldSpace(Ng));

    vec3f Ns = Ng;
    if (self.normal && sm.hasNormals) {
      Ns
        = (1.f-u-v)*self.normal[index.x]
        +      u   *self.normal[index.y]
        +        v *self.normal[index.z];
      Ns = normalize((vec3f)optixTransformNormalFromObjectToWorldSpace(Ns));
    }

    prd.Ns = Ns;
    prd.Ng = Ng;
    prd.uv = vec2f(u,v);
    // if (sm.colorTextureIsBaked) {
    //   const int bakedTextureRes = 16;
    //   const int primID = prd.primID;
    //   const int quadID = primID/2;
    //   const int atlas_x = quadID % 1024;
    //   const int atlas_y = quadID / 1024;
    //   const float u0 = atlas_x*bakedTextureRes / (1024.f*bakedTextureRes-1.f);
    //   const float v0 = atlas_y*bakedTextureRes / (1024.f....*bakedTextureRes-1.f);
    // } else
    if (sm.hasTexcoords) {
      prd.uv
          = (1.f-u-v) * vec2f(self.texcoord[index.x])
          +      u    * vec2f(self.texcoord[index.y])
          +        v  * vec2f(self.texcoord[index.z]);
    }
    
    const vec3f localP = (1.f-u-v)*A+u*B+v*C;
    prd.P = optixTransformPointFromObjectToWorldSpace(localP);
    prd.instID = (int)optixGetInstanceId();
    prd.colorTextureID = sm.colorTextureID;
  }

  OPTIX_CLOSEST_HIT_PROGRAM(TriMesh_shadow)()
  {
    auto &prd = owl::getPRD<int>();
    prd = 1;
  }
  
}
