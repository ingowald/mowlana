// ======================================================================== //
// Copyright 2019-2020 Ingo Wald                                            //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#pragma once

// ours
#include "cup/serialized/Scene.h"
#include "cup/pod/ImageTextures.h"
#include "cup/pod/MaterialData.h"
#include "cup/pod/MergedMesh.h"
#include "cup/tools/Lights.h"
// owl
#include "owl/owl.h"

namespace mowlana {

  using cup::serialized::Scene;
  using cup::pod::ImageTextureHost;
  using cup::pod::MergedMeshBuilder;
  
  using namespace owl;
  using namespace owl::common;

  struct Renderer {
    static std::string texturePath;
    
#if OPTIX_VERSION < 70100
    enum { MAX_INSTANCES_PER_GROUP = 16000000 };
#else
    enum { MAX_INSTANCES_PER_GROUP = 48000000 };
#endif
    Renderer(Scene::SP scene);

    void setSPP(int spp);
    void setMaxNumBounces(int mnb);
    void setShadeMode(int sm);
    int  getSPP() const { return spp; }
    void renderFrame();
    
    void updateCamera();
    void resizeFrameBuffer(const vec2i &newSize, void *fpPointer);
    void resetAccumulation();
    void setHeatMap(float scale);
    void setDebugPixel(const vec2i &pixelID);
    
    struct {
      vec3f screen_00;
      vec3f screen_du;
      vec3f screen_dv;
      vec3f lens_center;
      vec3f lens_du;
      vec3f lens_dv;
    } camera;
  private:
    struct PerObject {
      void buildAccel(Renderer *parent, pbrt::Object::SP object);
      void createShadingData(Renderer *parent);
      
      MergedMeshBuilder mm;
      OWLGeom           geom           = 0;
      OWLGroup          group          = 0;
      OWLBuffer         vertexBuffer   = 0;
      OWLBuffer         indexBuffer    = 0;

      OWLBuffer         normalBuffer   = 0;
      OWLBuffer         texcoordBuffer = 0;
      /* buffer of cup:optix::SubMesh'es */
      OWLBuffer         subMeshBuffer  = 0;
    };
    struct PerDevice {
      std::vector<cudaArray_t>         textureArrays;
      std::vector<cudaTextureObject_t> textureObjects;
    };
    
    std::vector<ImageTextureHost> hostTextures;
    std::vector<PerObject>        perObject;
    std::vector<PerDevice>        perDevice;
    /*! this is *one* buffer for all merged meshes - but be warned:
        since it contains actual cuda pointers and texture objects
        we'll have to fill it with different values for each device */
    OWLBuffer                     mergedMeshBuffer;
    
    void createRayGen();
    void createMissProg();

    void uploadMaterials();
    void createTextures();
    
    void buildInstances();
    void buildObjects();
    void uploadObjectShadingData();

    /*! create the 'world' object by creating the geoms, gruops,
        instances, etc, fro the 'scene' we're rendering */
    void createWorld();
    void createLights();
    void sync();

    OWLTexture loadTexture(const std::string &mapName);

    OWLContext owl { 0 };
    OWLGroup   world { 0 };
    OWLBuffer accumBuffer { 0 };
    OWLLaunchParams lp { 0 };
    OWLModule rayGenModule { 0 };
    OWLRayGen rayGen { 0 };
    OWLModule   missProgModule { 0 };
    OWLMissProg missProg0 { 0 };
    OWLMissProg missProg1 { 0 };
    OWLGeomType triMeshType { 0 };
    OWLModule triMeshModule { 0 };
    OWLBuffer materialsBuffer { 0 };
    OWLBuffer texturesBuffer { 0 };
    OWLBuffer textureObjectsBuffer { 0 };

    // number of devices we use for rendering
    int numDevices;
    vec2i fbSize { -1,-1 };
    int spp = 8;
    int accumID = 0;
    int shadeMode = 0;
    /*! max path depth - shold get overwritten by viewer upon start */
    int maxNumBounces = -1; 
    float heatMapScale = 0.f;
    Scene::SP scene;
  };
  
}
