// ======================================================================== //
// Copyright 2019-2020 Ingo Wald                                            //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#include "owl/common/parallel/parallel_for.h"
#include "cup/pod/MergedMesh.h"
#include "cup/pod/ImageTextures.h"
#include "cup/tools/Lights.h"
#include "render/Renderer.h"
// device types
#include "device/LaunchParams.h"
// #include "device/RayGen.h"
#include "device/MissProg.h"
#include "device/TriMesh.h"
#include <owl/helper/cuda.h>
// std
#include <set>
#include <samples/common/3rdParty/stb/stb_image.h>

extern "C" char owl_raygen_program[];
extern "C" char owl_missprog_program[];
extern "C" char owl_trimesh_programs[];

namespace mowlana {
  std::string Renderer::texturePath = "";

  using namespace cup;
  using namespace cup::pod;
  
  using cup::tools::QuadLightsList;
  
  void Renderer::sync()
  {
    assert(lp);
    for (int devID=0;devID<numDevices;devID++)
      cudaStreamSynchronize(owlParamsGetCudaStream(lp,devID));
  }

  Renderer::Renderer(Scene::SP scene)
    : scene(scene)
  {
    std::cout << "#mowlana: creating owl context" << std::endl;
    owl = owlContextCreate();
    owlContextSetRayTypeCount(owl,2);

    if (scene->pbrtScene->world->instances.size() > MAX_INSTANCES_PER_GROUP) {
      std::cout
        << "*************************************************************\n"
        << "WARNING: scene needs three-level instancing - that'll cost...\n"
        << "*************************************************************\n";
      owlSetMaxInstancingDepth(owl,2);
    }
    
    numDevices = owlGetDeviceCount(owl);
    perDevice.resize(numDevices);
    
    // init frame buffer and swapchain:
    resizeFrameBuffer(vec2i(1),nullptr);

    std::cout << "#mowlana: creating ray gen and miss progs" << std::endl;
    createRayGen();
    createMissProg();

    std::cout << "#mowlana: creating world" << std::endl;
    createWorld();

    std::cout << "#mowlana: creating lights" << std::endl;
    createLights();
    
    std::cout << "#mowlana: building programs and pipline" << std::endl;
    owlBuildPrograms(owl);
    owlBuildPipeline(owl);
    std::cout << "#mowlana: building SBT" << std::endl;
    owlBuildSBT(owl);
    std::cout << "#mowlana: everything built" << std::endl;

    setDebugPixel({-1,-1});
  }
  
  size_t numGeomGroupsCreated = 0;
  
  void Renderer::PerObject::buildAccel(Renderer *parent,
                                       pbrt::Object::SP object)
  {
    mm.build(parent->scene.get(),object->shapes);
    
    if (mm.index.empty())
      // no meshes in this object ...
      return;

    // -------------------------------------------------------
    // create owl data
    // -------------------------------------------------------
    geom = owlGeomCreate(parent->owl,parent->triMeshType);
    vertexBuffer
      = owlDeviceBufferCreate(parent->owl,OWL_FLOAT3,mm.vertex.size(),
                              mm.vertex.data());    
    
    indexBuffer
      = owlDeviceBufferCreate(parent->owl,OWL_INT4,mm.index.size(),
                              mm.index.data());
      
    owlTrianglesSetVertices(geom,vertexBuffer,mm.vertex.size(),sizeof(vec3f),0);
    owlTrianglesSetIndices(geom,indexBuffer,mm.index.size(),sizeof(vec4i),0);

    // ------------------------------------------------------------------
    // now, build the actual group
    // ------------------------------------------------------------------
    group = owlTrianglesGeomGroupCreate(parent->owl,1,&geom);
    owlGroupBuildAccel(group);

    // ------------------------------------------------------------------
    // finally, release the host arrays ... we no longer need those
    // ------------------------------------------------------------------
    mm.vertex.clear();
    mm.index.clear();
  }

  void Renderer::PerObject::createShadingData(Renderer *parent)
  {
    if (geom == 0)
      // there were no meshes in this object ...
      return;

    // -------------------------------------------------------
    // create remaining buffers:
    // -------------------------------------------------------
    normalBuffer
      = owlDeviceBufferCreate(parent->owl,OWL_FLOAT3,mm.normal.size(),
                              mm.normal.data());
    texcoordBuffer
      = owlDeviceBufferCreate(parent->owl,OWL_FLOAT2,mm.texcoord.size(),
                              mm.texcoord.data());
    subMeshBuffer
      = owlDeviceBufferCreate(parent->owl,OWL_USER_TYPE(pod::SubMesh),
                              mm.subMesh.size(),
                              /* NO DATA HERE: we're filling that in
                                 later on with device-specific
                                 data!!! */
                              nullptr);
    
    // -------------------------------------------------------
    // build per-device optix data
    // -------------------------------------------------------
    for (int deviceID=0;deviceID<parent->perDevice.size();deviceID++) {
      CUDA_CALL(SetDevice(deviceID));
      auto &thisDevice = parent->perDevice[deviceID];

      std::vector<cup::pod::SubMesh>  ourVersion(mm.subMesh.size());
      for (int smID=0;smID<ourVersion.size();smID++) {
        auto &pod  = mm.subMesh[smID];
        auto &ours = ourVersion[smID];
        ours = pod;
        ours.alphaTexture
          = (pod.alphaTextureID >= 0
             &&
             pod.alphaTextureID < thisDevice.textureObjects.size())
          ? thisDevice.textureObjects[pod.alphaTextureID]
          : 0;
      }
      cup::pod::SubMesh *d_ours
        = (cup::pod::SubMesh *)owlBufferGetPointer(subMeshBuffer,deviceID);
      CUDA_CALL(Memcpy(d_ours,ourVersion.data(),
                       ourVersion.size()*sizeof(ourVersion[0]),
                       cudaMemcpyDefault));
    }
    CUDA_CALL(SetDevice(0));
    
    // -------------------------------------------------------
    // now, assign them
    // -------------------------------------------------------
    owlGeomSetBuffer(geom,"vertex",  vertexBuffer);
    owlGeomSetBuffer(geom,"index",   indexBuffer);
    owlGeomSetBuffer(geom,"normal",  normalBuffer);
    owlGeomSetBuffer(geom,"texcoord",texcoordBuffer);
    owlGeomSetBuffer(geom,"subMesh", subMeshBuffer);

    // -------------------------------------------------------
    // and clear the host-side data
    // -------------------------------------------------------
    mm.subMesh.clear();
    mm.normal.clear();
    mm.texcoord.clear();
  }
  
  
  void Renderer::buildInstances()
  {
    auto &instances = scene->pbrtScene->world->instances;
    std::cout << "#mowlana: - building " << instances.size()
              << " instances" << std::endl;
    
    std::vector<OWLGroup> instanceGroups;
    for (size_t begin = 0; begin < instances.size();
         begin += MAX_INSTANCES_PER_GROUP) {
      size_t end     = std::min(begin+MAX_INSTANCES_PER_GROUP,
                                instances.size());
      size_t count   = end-begin;
      std::vector<uint32_t> instanceIDs(count);
      std::vector<affine3f> xfms(count);
      std::vector<OWLGroup> groups(count);
      parallel_for
        (count,
         [&](size_t childID){
           size_t instID = begin+childID;
           pbrt::Instance::SP inst = instances[instID];
           assert(inst);
           int objID = scene->getIdx(inst->object);
           OWLGroup childGroup = perObject[objID].group;
           assert(childGroup);
           groups[childID] = childGroup;
           xfms[childID] = (const affine3f&)inst->xfm;
           instanceIDs[childID] = instID;
           // owlInstanceGroupSetChild(group,childID,childGroup);
           // owlInstanceGroupSetTransform(group,childID,(const owl4x3f&)inst->xfm);
        },1024);
      OWLGroup group
        = owlInstanceGroupCreate(owl,count,
                                 groups.data(),
                                 instanceIDs.data(),
                                 (const float*)xfms.data(),
                                 OWL_MATRIX_FORMAT_OWL);
      owlGroupBuildAccel(group);
      instanceGroups.push_back(group);
    }
    if (instanceGroups.size() == 1) {
      this->world = instanceGroups[0];
    } else {
      this->world = owlInstanceGroupCreate(owl,instanceGroups.size());
      for (int i=0;i<instanceGroups.size();i++) {
        owlInstanceGroupSetChild(this->world,i,instanceGroups[i]);
      }
      owlGroupBuildAccel(this->world);
    }
  }


  /*! create the 'world' object by creating the geoms, gruops,
    instances, etc, fro the 'scene' we're rendering */
  void Renderer::createWorld()
  {
    OWLVarDecl vars[]
      = {
         { "subMesh", OWL_BUFPTR, OWL_OFFSETOF(cup::pod::MergedMesh,subMesh) },
         { "vertex", OWL_BUFPTR, OWL_OFFSETOF(cup::pod::MergedMesh,vertex) },
         { "normal", OWL_BUFPTR, OWL_OFFSETOF(cup::pod::MergedMesh,normal) },
         { "texcoord", OWL_BUFPTR, OWL_OFFSETOF(cup::pod::MergedMesh,texcoord) },
         { "index", OWL_BUFPTR, OWL_OFFSETOF(cup::pod::MergedMesh,index) },
         { /* sentinel */ nullptr }
    };
    triMeshModule
      = owlModuleCreate(owl,owl_trimesh_programs);
    triMeshType
      = owlGeomTypeCreate(owl,OWL_GEOM_TRIANGLES,sizeof(cup::pod::MergedMesh),
                          vars,-1);
    owlGeomTypeSetClosestHit(triMeshType,0,triMeshModule,"TriMesh");
    // owlGeomTypeSetClosestHit(triMeshType,1,triMeshModule,"TriMesh_shadow");
    owlGeomTypeSetAnyHit(triMeshType,0,triMeshModule,"TriMesh");
    // owlGeomTypeSetAnyHit(triMeshType,1,triMeshModule,"TriMesh");

    double t0 = getCurrentTime();
    buildObjects();
    buildInstances();
    double t1 = getCurrentTime();
    std::cout << "time to build : " << prettyDouble(t1-t0) << "s" << std::endl;

    owlParamsSetGroup(lp,"world",world);

    createTextures();
    uploadMaterials();
    uploadObjectShadingData();
  }

  OWLTexture Renderer::loadTexture(const std::string &mapName)
  {
    std::string fileName = texturePath + mapName;
    PRINT(fileName);
    
    int width,height,numChannels;
    unsigned char *texels = stbi_load(fileName.c_str(),
                                      &width,
                                      &height,
                                      &numChannels,
                                      STBI_rgb_alpha);
    if (!texels) {
      std::cout << OWL_TERMINAL_RED << "Warning: Could not read texture " << fileName
                << OWL_TERMINAL_DEFAULT << std::endl;
      return {};
    }

    std::vector<uint8_t> uc4(width*height*4);
    const uint8_t *in = texels;
    switch(numChannels) {
    case 1:
      for (int y=height-1;y>=0;--y) {
        uint8_t *out = uc4.data() + 4*y*width;
        for (int i=0;i<width;i++) {
          int alpha = *in; in += 4;
          *out++ = alpha;
          *out++ = alpha;
          *out++ = alpha;
          *out++ = alpha;
        }
      } break;
    case 3:
    case 4:
      for (int y=height-1;y>=0;--y) {
        uint8_t *out = uc4.data() + 4*y*width;
        for (int i=0;i<width;i++) {
          *out++ = *in++;//255;
          *out++ = *in++;
          *out++ = *in++;
          *out++ = *in++;
        }
      } break;
    default:
      throw std::runtime_error("unsupported num channels in image");
    };
      
    OWLTexture texture = owlTexture2DCreate(owl,OWL_TEXEL_FORMAT_RGBA8,
                                            width,height,uc4.data());
    free(texels);
    return texture;
  }
  
  void Renderer::createLights()
  {
    PRINT(scene->globalLights.size());

    pbrt::InfiniteLightSource::SP envLight;
    for (auto light : scene->globalLights) 
      envLight = light->as<pbrt::InfiniteLightSource>();
    
    if (envLight) {
      OWLTexture texture = loadTexture(envLight->mapName);
      owlParamsSetTexture(lp,"envLightTexture",texture);
      affine3f xfm = owl::common::rcp((const owl::affine3f&)envLight->transform);
      owlParamsSetRaw(lp,"envLightTransform",&xfm);
      vec3f power = (const vec3f&)envLight->scale * (const vec3f&)envLight->L;
      owlParamsSet3f(lp,"envLightPower",(const owl3f&)power);
    } else {
      owlParamsSetTexture(lp,"envLightTexture",(OWLTexture)0);
    }
    
    PRINT(scene->areaLights.size());
    QuadLightsList quadLights = tools::extractQuadLights(scene->areaLights);
    PRINT(quadLights.size());
  }
  
  void Renderer::buildObjects()
  {
    std::cout << "#mowlana: - building " << scene->objects.size()
              << " objects" << std::endl;
    perObject.resize(scene->objects.size());
    serial_for(perObject.size(),[&](size_t objID){
        perObject[objID].buildAccel(this,scene->objects[objID]);
      });
  }
  
  void Renderer::uploadObjectShadingData()
  {
    perObject.resize(scene->objects.size());
    serial_for(perObject.size(),[&](size_t objID){
        perObject[objID].createShadingData(this);
      });
  }
  
  void Renderer::createTextures()
  {
    if (texturePath == "") {
      std::cout << OWL_TERMINAL_RED << "WARNING: no texture path specified!"
                << OWL_TERMINAL_DEFAULT << std::endl;
      return;
    }

    loadImageTextures(hostTextures,scene,texturePath);

    OWLBuffer textureObjectsBuffer
      = owlDeviceBufferCreate(owl,OWL_USER_TYPE(cudaTextureObject_t),
                              hostTextures.size(),nullptr);
    for (int devID=0;devID<perDevice.size();devID++) {
      CUDA_CALL(SetDevice(devID));
      auto &pd = perDevice[devID];
      createTextureObjects(pd.textureArrays,
                           pd.textureObjects,
                           hostTextures);
      CUDA_CALL(Memcpy((void*)owlBufferGetPointer(textureObjectsBuffer,devID),
                       pd.textureObjects.data(),
                       pd.textureObjects.size()*sizeof(pd.textureObjects[0]),
                       cudaMemcpyHostToDevice));
    }
    CUDA_CALL(SetDevice(0));

    owlParamsSetBuffer(lp,"textureObjects",textureObjectsBuffer);
  }

  void Renderer::uploadMaterials()
  {
    std::vector<pod::Material> materials(scene->materials.size());
    for (int matID=0;matID<scene->materials.size();matID++) {
      materials[matID] = pod::createMaterial(scene.get(),scene->materials[matID]);
    }
    std::vector<pod::Texture> textures(scene->textures.size());
    for (int texID=0;texID<scene->textures.size();texID++) {
      textures[texID] = pod::createTexture(scene.get(),
                                           scene->textures[texID],
                                           hostTextures);
    }
    materialsBuffer = owlDeviceBufferCreate
      (owl,OWL_USER_TYPE(pod::Material),
       materials.size(),materials.data());
    texturesBuffer = owlDeviceBufferCreate
      (owl,OWL_USER_TYPE(pod::Texture),
       textures.size(),textures.data());
    
    owlParamsSetBuffer(lp,"materials",materialsBuffer);
    owlParamsSetBuffer(lp,"textures",texturesBuffer);
  }
  
  void Renderer::createRayGen()
  {
    OWLVarDecl vars[]
      = {
         { /* sentinel */ nullptr }
    };
    rayGenModule = owlModuleCreate(owl,owl_raygen_program);
    rayGen = owlRayGenCreate(owl,rayGenModule,"renderFrame",
                             0/*sizeof(device::RayGen)*/,vars,-1);
  }

  void Renderer::createMissProg()
  {
    OWLVarDecl vars[]
      = {
         { /* sentinel */ nullptr }
    };
    missProgModule = owlModuleCreate(owl,owl_missprog_program);
    missProg0       = owlMissProgCreate(owl,missProgModule,"miss",
                                        sizeof(dev::MissProg),vars,-1);
    missProg1       = owlMissProgCreate(owl,missProgModule,"miss_shadow",
                                        sizeof(dev::MissProg),vars,-1);
  }
  
  void Renderer::setSPP(int spp)
  {
    this->spp = spp;
    std::cout << "Samples per pixel now " << spp << std::endl;
    owlParamsSet1i(lp,"samplesPerPixel", spp);
  }
  
  void Renderer::setMaxNumBounces(int maxNumBounces)
  {
    this->maxNumBounces = maxNumBounces;
    std::cout << "max bounces per path " << maxNumBounces << std::endl;
    owlParamsSet1i(lp,"maxNumBounces", maxNumBounces);
  }
  
  void Renderer::renderFrame()
  {
    int thisAccumID = this->accumID++;
    owlParamsSet1i(lp,"accumID",thisAccumID);
    owlParamsSet1i(lp,"shadeMode", shadeMode);
    owlParamsSet1f(lp,"heatMapScale",heatMapScale);
    owlLaunch2D(rayGen,fbSize.x,fbSize.y,lp);
    sync();
    cudaDeviceSynchronize();
    
  }
    
  void Renderer::updateCamera()
  {
    owlParamsSet3f(lp,"camera.origin",   (const owl3f&)camera.lens_center);
    owlParamsSet3f(lp,"camera.screen_00",(const owl3f&)camera.screen_00);
    owlParamsSet3f(lp,"camera.screen_du",(const owl3f&)camera.screen_du);
    owlParamsSet3f(lp,"camera.screen_dv",(const owl3f&)camera.screen_dv);
  }
  
  void Renderer::resizeFrameBuffer(const vec2i &newSize, void *fbPointer)
  {
    cudaDeviceSynchronize();

    if (!lp) {
      OWLVarDecl lpVars[]
        = {
           { "envLightTexture", OWL_TEXTURE,  OWL_OFFSETOF(dev::LPs,envLightTexture) },
           { "envLightTransform", OWL_USER_TYPE(affine3f), OWL_OFFSETOF(dev::LPs,envLightTransform) },
           { "envLightPower", OWL_FLOAT3, OWL_OFFSETOF(dev::LPs,envLightPower) },
           { "heatMapScale", OWL_FLOAT,  OWL_OFFSETOF(dev::LPs,heatMapScale) },
           { "accumID", OWL_INT,  OWL_OFFSETOF(dev::LPs,accumID) },
           { "debugPixel.x", OWL_INT,  OWL_OFFSETOF(dev::LPs,debugPixel.x) },
           { "debugPixel.y", OWL_INT,  OWL_OFFSETOF(dev::LPs,debugPixel.y) },
           { "deviceID", OWL_DEVICE,  OWL_OFFSETOF(dev::LPs,deviceID) },
           { "numDevices", OWL_INT,  OWL_OFFSETOF(dev::LPs,numDevices) },
           { "shadeMode", OWL_INT,  OWL_OFFSETOF(dev::LPs,shadeMode) },
           { "samplesPerPixel", OWL_INT,  OWL_OFFSETOF(dev::LPs,samplesPerPixel) },
           { "maxNumBounces", OWL_INT,  OWL_OFFSETOF(dev::LPs,maxNumBounces) },
           { "fbSize",  OWL_INT2, OWL_OFFSETOF(dev::LPs,fbSize) },
           { "colorBuffer", OWL_RAW_POINTER, OWL_OFFSETOF(dev::LPs,colorBuffer) },
           { "accumBuffer", OWL_BUFPTR, OWL_OFFSETOF(dev::LPs,accumBuffer) },
           { "world",  OWL_GROUP, OWL_OFFSETOF(dev::LPs,world) },
           { "materials",  OWL_BUFPTR, OWL_OFFSETOF(dev::LPs,materialData.materials) },
           { "textures",  OWL_BUFPTR, OWL_OFFSETOF(dev::LPs,materialData.textures) },
           { "textureObjects",  OWL_BUFPTR, OWL_OFFSETOF(dev::LPs,materialData.textureObjects) },
           { "camera.origin",    OWL_FLOAT3, OWL_OFFSETOF(dev::LPs,camera.origin) },
           { "camera.screen_00", OWL_FLOAT3, OWL_OFFSETOF(dev::LPs,camera.screen_00) },
           { "camera.screen_du", OWL_FLOAT3, OWL_OFFSETOF(dev::LPs,camera.screen_du) },
           { "camera.screen_dv", OWL_FLOAT3, OWL_OFFSETOF(dev::LPs,camera.screen_dv) },
           { /* sentinel */ nullptr }
      };
      lp = owlParamsCreate(owl,sizeof(dev::LPs),lpVars,-1);
      owlParamsSet1f(lp,"heatMapScale",0.f);
      owlParamsSet1i(lp,"numDevices",owlGetDeviceCount(owl));
    }
    
    this->fbSize = newSize;
    
    if (accumBuffer)
      owlBufferDestroy(accumBuffer);
    accumBuffer = owlDeviceBufferCreate(owl,OWL_FLOAT4,fbSize.x*fbSize.y,nullptr);

    owlParamsSet1ul(lp,"colorBuffer",(uint64_t)fbPointer);
    owlParamsSetBuffer(lp,"accumBuffer",accumBuffer);
    owlParamsSet2i(lp,"fbSize",fbSize.x,fbSize.y);
  }    
  
  void Renderer::resetAccumulation()
  {
    accumID = 0;
  }

  void Renderer::setShadeMode(int sm)
  {
    shadeMode = sm;
    resetAccumulation();
  }
    
  void Renderer::setHeatMap(float scale)
  {
    std::cout << "setting heatmap scale to " << scale
              << " (use '<'/'>' to change)" << std::endl;
    heatMapScale = scale;
    resetAccumulation();
  }
  
  void Renderer::setDebugPixel(const vec2i &pixelID)
  {
    owlParamsSet1i(lp,"debugPixel.x",pixelID.x);
    owlParamsSet1i(lp,"debugPixel.y",pixelID.y);
  }
  
}
