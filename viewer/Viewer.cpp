// ======================================================================== //
// Copyright 2019-2020 Ingo Wald                                            //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#include "viewer/Viewer.h"
// eventually to go into 'apps/'
// #define STB_IMAGE_WRITE_IMPLEMENTATION 1
#include "samples/common/3rdParty/stb/stb_image_write.h"
#define STB_IMAGE_IMPLEMENTATION 1
#include "samples/common/3rdParty/stb/stb_image.h"

namespace mowlana {

  int Viewer::measure = 0;
  
  Viewer::Viewer(Renderer *renderer)
    : OWLViewer("m-owl-ana"),
      renderer(renderer)
  {}

  void Viewer::screenShot()
  {
    const uint32_t *fb
      = (const uint32_t*)fbPointer;
    
    const std::string fileName = screenShotFileName;
    std::vector<uint32_t> pixels;
    for (int y=0;y<fbSize.y;y++) {
      const uint32_t *line = fb + (fbSize.y-1-y)*fbSize.x;
      for (int x=0;x<fbSize.x;x++) {
        pixels.push_back(line[x] | (0xff << 24));
      }
    }
    stbi_write_png(fileName.c_str(),fbSize.x,fbSize.y,4,
                   pixels.data(),fbSize.x*sizeof(uint32_t));
    std::cout << "screenshot saved in '" << fileName << "'" << std::endl;
  }
    
  /*! this function gets called whenever the viewer widget changes
      camera settings */
  void Viewer::cameraChanged() 
  {
    inherited::cameraChanged();
    const viewer::SimpleCamera &camera = inherited::getCamera();
    
    const vec3f screen_du = camera.screen.horizontal / float(getWindowSize().x);
    const vec3f screen_dv = camera.screen.vertical   / float(getWindowSize().y);
    renderer->camera.screen_du = screen_du;
    renderer->camera.screen_dv = screen_dv;
    renderer->camera.screen_00 = camera.screen.lower_left;
    renderer->camera.lens_center = camera.lens.center;
    renderer->camera.lens_du = camera.lens.du;
    renderer->camera.lens_dv = camera.lens.dv;
    renderer->updateCamera();
    renderer->resetAccumulation();
  }
    
  /*! window notifies us that we got resized */
  void Viewer::resize(const vec2i &newSize) 
  {
    // ... tell parent to resize (also resizes the pbo in the wingdow)
    inherited::resize(newSize);
    
    renderer->resizeFrameBuffer(newSize,fbPointer);
      
    // ... and finally: update the camera's aspect
    setAspect(newSize.x/float(newSize.y));
      
    // update camera as well, since resize changed both aspect and
    // u/v pixel delta vectors ...
    updateCamera();
  }
    
  /*! gets called whenever the viewer needs us to re-render out widget */
  void Viewer::render() 
  {
    static double t_last = getCurrentTime();
    static double t_first = t_last;

    renderer->renderFrame();
      
    double t_now = getCurrentTime();
    static double avg_t = t_now-t_last;
    // if (t_last >= 0)
    avg_t = 0.8*avg_t + 0.2*(t_now-t_last);

    char title[1000];
    sprintf(title,"mowlana - %.2f FPS",(1.f/avg_t));
    // window->setTitle(title);
    glfwSetWindowTitle(this->handle,title);

    if (measure) {
      static int numFrames = 0;
      ++numFrames;
      if ((t_now - t_first) >= measure) {
        screenShot();
        std::cout << "MEASURE_FPS " << (numFrames/(t_now-t_first)) << std::endl;
        exit(0);
      }
    }
    t_last = t_now;
  }
    
  /*! this gets called when the user presses a key on the keyboard ... */
  void Viewer::key(char key, const vec2i &where)
  {
    switch (key) {
    case '0':
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
      renderer->setShadeMode(key - '0');
      break;
    case '!':
      screenShot();
      break;
    case '>':
      heatMapScale *= 1.5f;
      renderer->setHeatMap(heatMapScale);
      break;
    case '<':
      heatMapScale /= 1.5f;
      renderer->setHeatMap(heatMapScale);
      break;

    case '^':
      renderer->setDebugPixel(where);
      break;

    case '(':
      renderer->setSPP(std::max(renderer->getSPP()-1,1));
      break;
    case ')':
      renderer->setSPP(renderer->getSPP()+1);
      break;
      
    case 'h':
    case 'H':
      heatMapEnabled ^= 1;
      if (heatMapEnabled)
        renderer->setHeatMap(heatMapScale);
      else
        renderer->setHeatMap(0.f);
      break;
    case 'C': {
      auto &fc = getCamera();
      std::cout << "(C)urrent camera:" << std::endl;
      std::cout << "- from :" << fc.position << std::endl;
      std::cout << "- poi  :" << fc.getPOI() << std::endl;
      std::cout << "- upVec:" << fc.upVector << std::endl; 
      std::cout << "- frame:" << fc.frame << std::endl;
      std::cout.precision(10);
      std::cout << "cmdline: --camera "
                << fc.position.x << " "
                << fc.position.y << " "
                << fc.position.z << " "
                << fc.getPOI().x << " "
                << fc.getPOI().y << " "
                << fc.getPOI().z << " "
                << fc.upVector.x << " "
                << fc.upVector.y << " "
                << fc.upVector.z << " "
                << "-fovy " << fc.fovyInDegrees
                << std::endl;
    } break;
    default:
      inherited::key(key,where);
    };
  }
}
