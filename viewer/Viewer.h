// ======================================================================== //
// Copyright 2019-2020 Ingo Wald                                            //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#pragma once

#include "render/Renderer.h"
#include "samples/common/owlViewer/OWLViewer.h"

namespace mowlana {

  struct Viewer : public owl::viewer::OWLViewer {
    typedef OWLViewer inherited;

    Viewer(Renderer *renderer);

    void screenShot();
    
    // /*! this function gets called whenever the viewer widget changes camera settings */
    virtual void cameraChanged() override;
    
    /*! window notifies us that we got resized */
    virtual void resize(const vec2i &newSize) override;
    
    /*! gets called whenever the viewer needs us to re-render out widget */
    virtual void render() override;
    
    /*! this gets called when the user presses a key on the keyboard ... */
    virtual void key(char key, const vec2i &where);

    bool  heatMapEnabled = false;
    
#if MEASURE_ANYHIT_CALLS
    float heatMapScale = 1.f/100.f;
#else
    float heatMapScale = 1e-7f;
#endif

    /*! measure time, in seconds */
    static int measure;
    
    Renderer *const renderer;
    std::string screenShotFileName = "mowlana-screenshot.png";
  };
  
}
