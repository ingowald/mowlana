// ======================================================================== //
// Copyright 2019-2020 Ingo Wald                                            //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#include "viewer/Viewer.h"
#include "cup/tools/worldBounds.h"

namespace mowlana {

  struct {
    int spp = 1;
    int maxNumBounces = 3;
    int shadeMode = 9;
    struct {
      vec3f vp = vec3f(0.f);
      vec3f vu = vec3f(0.f);
      vec3f vi = vec3f(0.f);
      float fovy = 70;
    } camera;
    vec2i windowSize  = vec2i(0,0);
    float windowScale = 1.f;
    std::string screenShotFileName;
  } cmdline;
  
  void usage(const std::string &err)
  {
    if (err != "")
      std::cout << OWL_TERMINAL_RED << "\nFatal error: " << err
                << OWL_TERMINAL_DEFAULT << std::endl << std::endl;

    std::cout << "Usage: mowlana /path/to/moana.pbf [-t bakedPtexPathPrefix]" << std::endl;
    std::cout << std::endl;
    exit(1);
  }
  
  vec2i maxWindowSize(vec2i desiredSize)
  {
    vec2i screenSize 
      = owl::viewer::OWLViewer::getScreenSize();
      - vec2i(30);
    float downScale = 1.f;
    downScale = std::max(downScale,screenSize.x/float(desiredSize.x));
    downScale = std::max(downScale,screenSize.y/float(desiredSize.y));
    return vec2i((1.f/downScale) * vec2f(desiredSize));
  }

  extern "C" int main(int argc, char **argv)
  {
    std::string inFileName;

    for (int i=1;i<argc;i++) {
      const std::string arg = argv[i];
      if (arg[0] != '-') {
        inFileName = arg;
      }
      else if (arg == "--measure") {
        Viewer::measure = 10;
      }
      else if (arg == "--measure-time") {
        Viewer::measure = std::stoi(argv[++i]);
        std::cout << "going to measure for " << Viewer::measure << " seconds..." << std::endl;
      }
      else if (arg == "--shade-mode" || arg == "-sm") {
        cmdline.shadeMode = std::stoi(argv[++i]);
      }
      else if (arg == "--max-num-bounces" || arg == "-mnb") {
        cmdline.maxNumBounces = std::stoi(argv[++i]);
      }
      else if (arg == "-fovy") {
        cmdline.camera.fovy = std::atof(argv[++i]);
      }
      else if (arg == "--camera") {
        cmdline.camera.vp.x = std::stof(argv[++i]);
        cmdline.camera.vp.y = std::stof(argv[++i]);
        cmdline.camera.vp.z = std::stof(argv[++i]);
        cmdline.camera.vi.x = std::stof(argv[++i]);
        cmdline.camera.vi.y = std::stof(argv[++i]);
        cmdline.camera.vi.z = std::stof(argv[++i]);
        cmdline.camera.vu.x = std::stof(argv[++i]);
        cmdline.camera.vu.y = std::stof(argv[++i]);
        cmdline.camera.vu.z = std::stof(argv[++i]);
      }
      else if (arg == "-win"  || arg == "--win" || arg == "--size") {
        cmdline.windowSize.x = std::atoi(argv[++i]);
        cmdline.windowSize.y = std::atoi(argv[++i]);
      }
      else if (arg == "--window-scale" || arg == "-ws") {
        cmdline.windowScale = std::atof(argv[++i]);
      }
      else if (arg == "-o") {
        cmdline.screenShotFileName = argv[++i];
      }
      else if (arg == "-t") {
        Renderer::texturePath = argv[++i];
      }
      else if (arg == "--measure") {
        Viewer::measure = true;
      }
      else if (arg == "-spp" || arg == "--spp") {
        cmdline.spp = std::stoi(argv[++i]);
      }
      else
        usage("unknown cmdline arg '"+arg+"'");
    }
    
    if (inFileName == "")
      usage("no filename specified");

    cup::serialized::Scene::SP scene
      = cup::serialized::Scene::load(inFileName);
    auto input = scene->pbrtScene;
    std::cout << "#mowlana: computing world bounds" << std::endl;
    const box3f sceneBounds
      = cup::tools::getWorldBounds(scene->pbrtScene,/*exclude lights:*/true);
    
    Renderer renderer(scene);
    
    vec2i windowSize = cmdline.windowSize;
    if (windowSize == vec2i(0)) {
      // not set explicitly - set one
      windowSize = input->film
        ? maxWindowSize(vec2i(cmdline.windowScale * vec2f((const vec2i &)input->film->resolution)))
        : vec2i(1200,800);
    }
    Viewer viewer(&renderer);

    if (cmdline.screenShotFileName != "")
      viewer.screenShotFileName = cmdline.screenShotFileName;

    renderer.setSPP(cmdline.spp);
    renderer.setShadeMode(cmdline.shadeMode);
    renderer.setMaxNumBounces(cmdline.maxNumBounces);
    
    viewer.enableInspectMode(/* valid range of poi*/box3f(),//sceneBounds,
                             /* min distance      */1e-3f,
                             /* max distance      */1e8f);
    viewer.enableFlyMode();
    
    if (cmdline.camera.vu != vec3f(0.f)) {
      viewer.setCameraOrientation(/*origin   */cmdline.camera.vp,
                                  /*lookat   */cmdline.camera.vi,
                                  /*up-vector*/cmdline.camera.vu,
                                  /*fovy(deg)*/cmdline.camera.fovy);
    } else if (input->cameras.empty()) {
      std::cout << "No camera in model, nor on command line - generating from bounds ...."
                << std::endl;
      viewer.setCameraOrientation(/*origin   */
                                  sceneBounds.center()
                                  + vec3f(-.3f,.7f,+1.f)*sceneBounds.span(),
                                  /*lookat   */sceneBounds.center(),
                                  /*up-vector*/vec3f(0.f, 1.f, 0.f),
                                  /*fovy(deg)*/cmdline.camera.fovy);
    } else {
      std::cout << "Camera from model!"
                << std::endl;
      pbrt::Camera::SP cam = input->cameras[0];
      const vec3f lookFrom = (vec3f&)cam->frame.p;
      const vec3f lookAt   = (vec3f&)cam->frame.p + 0.5f*length(sceneBounds.size())*(vec3f&)cam->frame.l.vz;
      const vec3f lookUp   = viewer::getUpVector((vec3f&)cam->frame.l.vy);

      float fovy = cam->fov;
      if (input->film) {
        const vec2i res = (const vec2i &)input->film->resolution;
        if (res.y > res.x)
          fovy *= (res.y/float(res.x));
      }
      
      viewer.setCameraOrientation(/*origin   */lookFrom,
                                  /*lookat   */lookAt,
                                  /*up-vector*/lookUp,
                                  /*fovy(deg)*/fovy);
    }
    viewer.setWorldScale(.1f*length(sceneBounds.span()));
    viewer.showAndRun();

    return 0;
  }
  
}
